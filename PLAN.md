need the following services:

- showcase the dependency chain pattern in 3 independent service calls

- showcase the isolation pattern in a service call that generates 2 independent actions

- showcase the trigger-get pattern with a service call/logic gate

- showcase the request-response-loop pattern

