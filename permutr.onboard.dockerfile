### Stage 1 - Pull sourcecode down from Gitlab

FROM node:latest as REPO
LABEL Author = "Ryan Barriger"

### Install git
RUN apt-get update && apt-get install -y git-core

### Configure Git
RUN git config --global user.name "ibbey"
RUN git config --global user.email "ryan.barriger@outlook.com"

### Set up the sandbox directory and clone the repository
RUN mkdir /usr/sandbox && cd /usr/sandbox \
    && git clone http://gitlab.com/Ibbey/permutr-angular

WORKDIR /usr/sandbox/permutr-angular

### Force an install of node-sass - AOT will complain/reject later on
RUN npm install && npm install node-sass@latest

RUN $(npm bin)/npm run build:local

### Stage 2 - Setup and copy the built Permutr Application to the nginx image
FROM nginx:alpine

COPY --from=REPO usr/sandbox/permutr-angular/nginx/nginx.local.conf /etc/nginx/nginx.conf

### Remove any lingering files or default files that were installed by the base image
RUN rm -rf /usr/share/nginx/html/*

COPY --from=REPO /usr/sandbox/permutr-angular/dist/local /usr/share/nginx/html

### Kick the nginx proxy
CMD [ "nginx", "-g", "daemon off;" ]

