### Stage 1 - Build
FROM node:latest as builder
LABEL Author="Ryan Barriger"

COPY package.json package.json

RUN npm install && npm install node-sass@latest && mkdir /permutr-app && mv ./node_modules ./permutr-app

WORKDIR /permutr-app

COPY . .

RUN $(npm bin)/npm run build:local

### Stage 2 - Setup

FROM nginx:alpine

COPY nginx/nginx.local.conf /etc/nginx/nginx.conf

RUN rm -rf /user/share/nginx/html/*

COPY --from=builder /permutr-app/dist/local /usr/share/nginx/html

CMD [ "nginx", "-g", "daemon off;" ]

