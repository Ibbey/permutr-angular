export interface IEnvironmentMetaData {
    jEchoUri: string;
    pEchoUri: string;
    inventoryUri: string;
    dependencyChainUri: string;
    isolationUri: string;
    triggerGetUri: string;
    requestResponseLoopUri: string;

    version: string;
    production: boolean;
}
