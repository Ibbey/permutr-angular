import { IEnvironmentMetaData } from './environment.contract';

export const environment: IEnvironmentMetaData = {
    jEchoUri: 'local',
    pEchoUri: 'local',
    inventoryUri: 'local',
    dependencyChainUri: 'local',
    isolationUri: 'local',
    triggerGetUri: 'local',
    requestResponseLoopUri: 'local',
    version: '1.0.0',
    production: true
};
