import { IEnvironmentMetaData } from './environment.contract';

export const environment: IEnvironmentMetaData = {
    jEchoUri: 'http://localhost:8081/api/permutr/v1',
    pEchoUri: 'http://localhost:8081/analytics/permutr/v1',
    inventoryUri: 'http://localhost:8081/api/permutr/v1',
    dependencyChainUri: 'local',
    isolationUri: 'local',
    triggerGetUri: 'local',
    requestResponseLoopUri: 'local',
    version: '1.0.0',
    production: false
};
