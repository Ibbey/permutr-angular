import { IEnvironmentMetaData } from './environment.contract';

export const environment: IEnvironmentMetaData = {
    jEchoUri: 'http://localhost:8080/api/permutr/v1',
    pEchoUri: 'http://localhost:8081/analytics/permutr/v1',
    inventoryUri: 'http://localhost/8080/api/permutr/v1',
    dependencyChainUri: 'http://localhost:8080/api/permutr/v1',
    isolationUri: 'http://localhost:8080/api/permutr/v1',
    triggerGetUri: 'http://localhost:8080/api/permutr/v1',
    requestResponseLoopUri: 'http://localhost:8080/api/permutr/v1',
    version: '1.0.0',
    production: true
};
