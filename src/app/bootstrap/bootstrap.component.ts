import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'permutr-bootstrap',
    templateUrl: './bootstrap.component.html',
    styleUrls: [ './bootstrap.component.scss' ]
})
export class PermutrBootstrapComponent implements OnInit, OnDestroy {

    // Constructor ********************************************************************************
    constructor() {

    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {

    }

    public ngOnDestroy(): void {

    }
}
