import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PermutrShellComponent } from './shell/shell.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', component: PermutrShellComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
