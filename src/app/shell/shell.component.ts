import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrGlobalState } from '../store/state/permutr.state';

import { NavigationOptions } from '@models/navigation/navigation-options';

import * as fromNavigation from '@store/selectors/navigation.selectors';

@Component({
    selector: 'permutr-shell',
    templateUrl: './shell.component.html',
    styleUrls: [ './shell.component.scss' ]
})
export class PermutrShellComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _currentNavigation$: Observable<NavigationOptions>;
    private _currentNavigation: NavigationOptions;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get currentNavigation(): NavigationOptions {
        return this._currentNavigation;
    }

    public NavigationOptions = NavigationOptions;

    // Constructor ********************************************************************************
    constructor(store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
        this._currentNavigation = NavigationOptions.HOME;

        this._currentNavigation$ = this._store.pipe(select(fromNavigation.getCurrentNavigation));
    }

    private subscribe(): void {
        if (this._currentNavigation$ !== undefined) {
            this._subscriptions.push(this._currentNavigation$.subscribe(x => this._currentNavigation = x));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }
}
