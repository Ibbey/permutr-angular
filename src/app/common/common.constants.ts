export const InventoryTypes = [
    'Artwork',
    'Figurine',
    'Statue',
    'Toy',
    'Other'
];

export const InventoryStatusOptions = [
    'Held',
    'For Sale',
    'Pending Sale',
    'Sold'
];
