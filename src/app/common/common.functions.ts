export function coerceBoolean(value: any): boolean {
    if (typeof(value) === 'boolean') {
        return value;
    }
    return !!value;
}

export function coerceNumber(value: any): number {
    if (typeof(value) === 'number') {
        return value;
    }
    return Number(value);
}

export function dateEquals(date1: Date, date2: Date): boolean {
    if (date1 === undefined || date2 === undefined) {
        return false;
    }

    if (date1.getFullYear() === date2.getFullYear()) {
        if (date1.getMonth() === date2.getMonth()) {
            if (date1.getDate() === date2.getDate()) {
                return true;
            }
        }
    }

    return false;
}

export function formatCurrency(value: any, precision: number = 2, decimal: string = '.', separator: string = ','): string {
    try {
        precision = Math.abs(precision);
        precision = isNaN(precision) ? 2 : precision;

        const i = parseInt(value = Math.abs(Number(value) || 0).toFixed(precision), 10).toString();
        const j = (i.length > 3) ? i.length % 3 : 0;

        const valueString = '$' + (j ? i.substr(0, j) + separator : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + separator) +
               (precision ? decimal + Math.abs(value - <any>i).toFixed(precision).slice(2) : '');

        if (value < 0) {
            return '(' + valueString + ')';
        }

        return valueString;

    } catch (exception) {
        console.error(exception);
    }
}

export function pureCollectionAdd(item: any, source: any[]): any[] {
    if (source === undefined) {
        return [ item ];
    }
    if (item !== undefined && source !== undefined) {
        const clone = source.slice(0);
        clone.push(item);

        return clone;
    }

    return undefined;
}
