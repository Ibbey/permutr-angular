import { InjectionToken } from '@angular/core';

import { ICrossComponentService } from './cross-component-service.contract';

export const CrossComponentServiceToken = new InjectionToken<ICrossComponentService>('CrossComponentService');
