import { Observable } from 'rxjs';

export interface ICrossComponentService {

    message$: Observable<string>;

    send(message: string): void;

    clear(): void;
}
