import { Injectable } from '@angular/core';
import { Observable, Subject} from 'rxjs';

import { ICrossComponentService } from './cross-component-service.contract';

@Injectable()
export class CrossComponentService implements ICrossComponentService {
    // Member Variables ***************************************************************************
    private _messageSource: Subject<string>;
    private _message$: Observable<string>;

    // Public Properties **************************************************************************
    public get message$(): Observable<string> {
        return this._message$;
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Interface Methods **************************************************************************
    public send(message: string): void {
        if (this._messageSource !== undefined) {
            this._messageSource.next(message);
        }
    }

    public clear(): void {
        if (this._messageSource !== undefined) {
            this._messageSource.next(undefined);
        }
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._messageSource = new Subject<string>();
        this._message$ = this._messageSource.asObservable();
    }
}
