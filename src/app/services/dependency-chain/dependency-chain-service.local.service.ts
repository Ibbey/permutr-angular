import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { IDependencyChainService } from './dependency-chain-service.contract';

export class LocalDependencyChainService implements IDependencyChainService {
    // Member Variables ***************************************************************************

    // Constructor ********************************************************************************
    constructor () {

    }

    // Interface Methods **************************************************************************
    public getDependencyA(): Observable<any> {
        const value = {
            id: 1,
            name: 'dependency chain A',
            content: 'First block of data'
        };

        return of(value).pipe(delay(2000));
    }

    public getDependencyB(): Observable<any> {
        const value = {
            id: 2,
            name: 'dependency chain B',
            content: 'Second block of data'
        };

        return of(value).pipe(delay(1000));
    }

    public getDependencyC(): Observable<any> {
        const value = {
            id: 3,
            name: 'dependency chain C',
            content: 'Third block of data'
        };

        return of(value).pipe(delay(5000));
    }
}
