import { InjectionToken } from '@angular/core';

import { IDependencyChainService } from './dependency-chain-service.contract';

export const DependencyChainServiceToken = new InjectionToken<IDependencyChainService>('DependencyChainService');
