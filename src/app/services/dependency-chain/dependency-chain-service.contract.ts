import { Observable } from 'rxjs';

export interface IDependencyChainService {

    getDependencyA(): Observable<any>;

    getDependencyB(): Observable<any>;

    getDependencyC(): Observable<any>;
}
