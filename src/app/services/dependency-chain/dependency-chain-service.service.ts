import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { IDependencyChainService } from './dependency-chain-service.contract';

export class DependencyChainService implements IDependencyChainService {
    // Constants **********************************************************************************
    private static chainAUri = '/dependencychain/a';
    private static chainBUri = '/dependencychain/b';
    private static chainCUri = '/dependencychain/c';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _baseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, baseUri: string) {
        this._http = http;
        this._baseUri = baseUri;
    }

    // Interface Methods **************************************************************************
    public getDependencyA(): Observable<any> {
        const uri = this._baseUri + DependencyChainService.chainAUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public getDependencyB(): Observable<any> {
        const uri = this._baseUri + DependencyChainService.chainBUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public getDependencyC(): Observable<any> {
        const uri = this._baseUri + DependencyChainService.chainCUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }
}
