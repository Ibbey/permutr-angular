import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { ITriggerGetService } from './trigger-get-service.contract';

export class LocalTriggerGetService implements ITriggerGetService {

    public getValueA(): Observable<any> {
        const value = {
            id: 1,
            name: 'Value A',
            content: 'Content block received from source A'
        };
        return of(value).pipe(delay(1000));
    }

    public getValueB(): Observable<any> {
        const value = {
            id: 2,
            name: 'Value B',
            content: 'Content block received from source B'
        };
        return of(value).pipe(delay(1000));
    }

    public getValueC(): Observable<any> {
        const value = {
            id: 3,
            name: 'Value C',
            content: 'Content block received from source C'
        };
        return of(value).pipe(delay(1000));
    }

    public getValueD(): Observable<any> {
        const value = {
            id: 4,
            name: 'Value D',
            content: 'Content block received from source D'
        };
        return of(value).pipe(delay(1000));
    }
}
