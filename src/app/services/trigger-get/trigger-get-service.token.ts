import { InjectionToken } from '@angular/core';

import { ITriggerGetService } from './trigger-get-service.contract';

export const TriggerGetServiceToken = new InjectionToken<ITriggerGetService>('TriggerGetService');
