import { Observable } from 'rxjs';

export interface ITriggerGetService {

    getValueA(): Observable<any>;

    getValueB(): Observable<any>;

    getValueC(): Observable<any>;

    getValueD(): Observable<any>;
}
