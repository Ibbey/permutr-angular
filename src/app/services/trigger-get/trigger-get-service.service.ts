import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ITriggerGetService } from './trigger-get-service.contract';

export class TriggerGetService implements ITriggerGetService {
    // Constants **********************************************************************************
    private static getAUri = '/triggerget/a';
    private static getBUri = '/triggerget/b';
    private static getCUri = '/triggerget/c';
    private static getDUri = '/triggerget/d';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _baseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, baseUri: string) {
        this._http = http;
        this._baseUri = baseUri;
    }

    // Interface Methods **************************************************************************
    public getValueA(): Observable<any> {
        const uri = this._baseUri + TriggerGetService.getAUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public getValueB(): Observable<any> {
        const uri = this._baseUri + TriggerGetService.getBUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public getValueC(): Observable<any> {
        const uri = this._baseUri + TriggerGetService.getCUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public getValueD(): Observable<any> {
        const uri = this._baseUri + TriggerGetService.getDUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }
}
