import { InjectionToken } from '@angular/core';

import { IInventoryService } from './inventory-service.contract';

export const InventoryServiceToken = new InjectionToken<IInventoryService>('InventoryService');
