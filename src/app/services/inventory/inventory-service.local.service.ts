import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { InventoryCollection, InventoryItem, IInventoryItem } from '@models/inventory/_inventory.barrel';

import { IInventoryService } from './inventory-service.contract';


export class LocalInventoryService implements IInventoryService {
    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;

    // Constructor ********************************************************************************
    constructor (http: HttpClient) {
        this._http = http;
    }

    // Interface Methods **************************************************************************
    public getCollection(filter: string): Observable<InventoryCollection> {
        const localData = '../../../assets/data/inventory-items.json';

        return this._http.get(localData).pipe(
            map((response: any) => {
                if (response !== null) {
                    const collection = new InventoryCollection();
                    for (let i = 0; i < response.length; i++) {
                        const item = new InventoryItem(<IInventoryItem>response[i]);
                        collection.add(item);
                    }
                    return collection;
                }
            }),
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public addItem(item: InventoryItem): Observable<boolean> {
        return undefined;
    }

    public removeItem(item: InventoryItem): Observable<boolean> {
        return undefined;
    }
}
