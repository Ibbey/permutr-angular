import { Observable } from 'rxjs';

import { InventoryCollection, InventoryItem } from '@models/inventory/_inventory.barrel';

export interface IInventoryService {

    getCollection(filter: string): Observable<InventoryCollection>;

    addItem(item: InventoryItem): Observable<boolean>;

    removeItem(item: InventoryItem): Observable<boolean>;

}
