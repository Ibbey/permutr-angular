import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { InventoryCollection, InventoryItem } from '@models/inventory/_inventory.barrel';

import { IInventoryService } from './inventory-service.contract';

export class InventoryService implements IInventoryService {
    // Constants **********************************************************************************
    private static collectionUri = '/inventory/collection';
    private static addUri = '/inventory/add';
    private static removeUri = '/inventory/remove';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _baseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, baseUri: string) {
        this._http = http;
        this._baseUri = baseUri;
    }

    // Interface Methods **************************************************************************
    public getCollection(filter: string): Observable<InventoryCollection> {
        return this._http.get(this._baseUri + InventoryService.collectionUri).pipe(
            map((response: HttpResponse<Object>) => {
                if (response.ok === true) {
                    return response.body as InventoryCollection;
                }
            }),
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public addItem(item: InventoryItem): Observable<boolean> {
        return this._http.post(this._baseUri + InventoryService.addUri, item).pipe(
            map((response: HttpResponse<Object>) => {
                if (response.ok === true) {
                    return true;
                }

                return false;
            }),
            catchError(error => {
                console.error(error);
                return of(false);
            })
        );
    }

    public removeItem(item: InventoryItem): Observable<boolean> {
        return this._http.post(this._baseUri + InventoryService.removeUri, item).pipe(
            map((response: HttpResponse<Object>) => {
                if (response.ok === true) {
                    return true;
                }

                return false;
            }),
            catchError(error => {
                console.error(error);
                return of(false);
            })
        );
    }
}
