import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { cold } from 'jasmine-marbles';

import { EchoService } from './echo-service.service';
import { IEcho } from '@models/echo/echo';

const BASE_URL = 'http://permutr-test/v1';

function echoServiceFactory(http: HttpClient): EchoService {
    return new EchoService(http, BASE_URL, BASE_URL);
}

describe('Echo Service', () => {

    let service: EchoService;
    let httpClient: HttpClient;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: HttpClient,
                    useValue: {
                        get: jest.fn()
                    }
                },
                {
                    provide: EchoService,
                    useFactory: echoServiceFactory,
                    deps: [ HttpClient ]
                }
            ]
        });

        httpClient = TestBed.get(HttpClient);
        service = TestBed.get(EchoService);

        jest.spyOn(console, 'error').mockImplementation(() => undefined);
    });

    it('should instantiate properly', () => {
        expect(service).toBeDefined();
    });

    it('should echo the java server properly', () => {

        const echo: IEcho = <IEcho>{
            id: 1,
            message: 'dummy message',
            source: 'dummy source',
            timestamp: new Date()
        };
        const expected = cold('-a|', { a: echo });
        const message = 'test';

        httpClient.get = jest.fn(() => expected);

        expect(service.pingJava(message)).toBeObservable(expected);
        expect(httpClient.get).toHaveBeenCalledWith(`${BASE_URL + '/jecho?message='}${message}`);

    });

    it('should echo the python server properly', () => {

        const echo: IEcho = <IEcho>{
            id: 1,
            message: 'dummy message',
            source: 'dummy source',
            timestamp: new Date()
        };
        const expected = cold('-a|', { a: echo });
        const message = 'test';

        httpClient.get = jest.fn(() => expected);

        expect(service.pingPython(message)).toBeObservable(expected);
        expect(httpClient.get).toHaveBeenCalledWith(`${BASE_URL + '/pecho?message='}${message}`);

    });
});
