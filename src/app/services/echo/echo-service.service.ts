import { HttpClient  } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { IEcho } from '@models/echo/echo';

import { IEchoService } from './echo-service.contract';


export class EchoService implements IEchoService {
    // Constants **********************************************************************************
    private static javaEchoUri = '/jecho';
    private static pythonEchoUri = '/pecho';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _javaBaseUri: string;
    private readonly _pythonBaseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, javaBaseUri: string, pythonBaseUri: string) {
        this._http = http;
        this._javaBaseUri = javaBaseUri;
        this._pythonBaseUri = pythonBaseUri;
    }

    // Interface Methods **************************************************************************
    public pingJava(message: string): Observable<IEcho> {
        const uri = this.getEchoUri(this._javaBaseUri, EchoService.javaEchoUri, message);
        return this._http.get(uri).pipe(
            map(response => {
                return response as IEcho;
            }),
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public pingPython(message: string): Observable<IEcho> {
        const uri = this.getEchoUri(this._pythonBaseUri, EchoService.pythonEchoUri, message);
        return this._http.get(uri).pipe(
            map(response => {
                return response as IEcho;
            }),
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    // Private Methods ****************************************************************************
    private getEchoUri(base: string, path: string, param: string): string {
        return base + path + '?message=' + param;
    }
}
