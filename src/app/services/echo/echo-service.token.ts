import { InjectionToken } from '@angular/core';

import { IEchoService } from './echo-service.contract';

export const EchoServiceToken = new InjectionToken<IEchoService>('EchoService');
