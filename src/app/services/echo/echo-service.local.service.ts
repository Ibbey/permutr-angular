import { Observable, of } from 'rxjs';

import { IEchoService } from './echo-service.contract';

import { IEcho } from '@models/echo/_echo.barrel';

export class LocalEchoService implements IEchoService {

    // Interface Methods **************************************************************************
    public pingJava(message: string): Observable<IEcho> {

        const echo = <IEcho>{
            id: 100,
            message: message,
            source: 'Powered by Java',
            timestamp: new Date(Date.now())
        };
        return of(echo);
    }

    public pingPython(message: string): Observable<IEcho> {

        const echo = <IEcho>{
            id: 100,
            message: message,
            source: 'Powered by Python',
            timestamp: new Date(Date.now())
        };
        return of(echo);
    }
}
