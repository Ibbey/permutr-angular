import { Observable } from 'rxjs';

import { IEcho } from '@models/echo/_echo.barrel';

export interface IEchoService {

    pingJava(message: string): Observable<IEcho>;

    pingPython(message: string): Observable<IEcho>;
}
