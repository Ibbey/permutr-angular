import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { IRequestResponseLoopService } from './request-response-loop-service.contract';

export class RequestResponseLoopService implements IRequestResponseLoopService {
    // Constants **********************************************************************************
    private static requestUri = '/requestresponseloop/';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _baseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, baseUri: string) {
        this._http = http;
        this._baseUri = baseUri;
    }

    // Interface Methods **************************************************************************
    public getResponse(request: any): Observable<any> {
        const uri = this._baseUri + RequestResponseLoopService.requestUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }
}
