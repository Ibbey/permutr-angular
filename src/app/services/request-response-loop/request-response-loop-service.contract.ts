import { Observable } from 'rxjs';

export interface IRequestResponseLoopService {

    getResponse(request: any): Observable<any>;
}
