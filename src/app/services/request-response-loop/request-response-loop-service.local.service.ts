import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { IRequestResponseLoopService } from './request-response-loop-service.contract';

export class LocalRequestResponseLoopService implements IRequestResponseLoopService {

    // Interface Methods **************************************************************************
    public getResponse(request: any): Observable<any> {
        const response = {
            id: request.id,
            name: request.key,
            content: 'Asynchronous Response'
        };

        return of(response).pipe(delay(this.getRandomDelay(1000, 3000)));
    }

    // Private Methods ****************************************************************************
    private getRandomDelay(minimum: number, maximum: number): number {
        return Math.floor(Math.random() * (maximum - minimum + 1) + minimum);
    }
}
