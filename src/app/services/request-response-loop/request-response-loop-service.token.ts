import { InjectionToken } from '@angular/core';

import { IRequestResponseLoopService } from './request-response-loop-service.contract';

export const RequestResponseLoopServiceToken = new InjectionToken<IRequestResponseLoopService>('RequestResponseLoopService');
