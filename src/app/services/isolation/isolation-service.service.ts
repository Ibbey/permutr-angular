import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { IIsolationService } from './isolation-service.contract';

export class IsolationService implements IIsolationService {
    // Constants **********************************************************************************
    private static isolationAUri = '/isolation/a';
    private static isolationBUri = '/isolation/b';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _baseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, baseUri: string) {
        this._http = http;
        this._baseUri = baseUri;
    }

    // Interface Methods **************************************************************************
    public getIsolationA(): Observable<any> {
        const uri = this._baseUri + IsolationService.isolationAUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    public getIsolationB(): Observable<any> {
        const uri = this._baseUri + IsolationService.isolationBUri;
        return this._http.get(uri).pipe(
            catchError(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }
}
