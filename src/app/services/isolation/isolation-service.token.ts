import { InjectionToken } from '@angular/core';

import { IIsolationService } from './isolation-service.contract';

export const IsolationServiceToken = new InjectionToken<IIsolationService>('IsolationService');
