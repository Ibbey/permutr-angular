import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { IIsolationService } from './isolation-service.contract';

export class LocalIsolationService implements IIsolationService {

    public getIsolationA(): Observable<any> {
        const value = {
            id: 1,
            name: 'Isolation A',
            content: 'Block of data retrieved via source A'
        };
        return of(value).pipe(delay(2000));
    }

    public getIsolationB(): Observable<any> {
        const value = {
            id: 2,
            name: 'Isolation B',
            content: 'Block of data retrieved via source B'
        };
        return of(value).pipe(delay(2000));
    }
}
