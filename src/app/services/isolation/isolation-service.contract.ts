import { Observable } from 'rxjs';

export interface IIsolationService {

    getIsolationA(): Observable<any>;

    getIsolationB(): Observable<any>;
}
