// Angular Modules ********************************************************************************
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// @ngrx Modules **********************************************************************************
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// Angular Material Modules ***********************************************************************
import { MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatIconModule,
         MatRadioModule, MatSidenavModule, MatTabsModule, MatToolbarModule, MatDialogModule, MatSlideToggleModule,
         MatSelectModule, MatProgressBarModule, MatProgressSpinnerModule, MatFormFieldModule, MatAutocompleteModule,
         MatInputModule, MatTooltipModule, MatExpansionModule, MatSnackBarModule, MatOptionModule, MatTableModule,
         MatDatepickerModule, MatSnackBar, RippleGlobalOptions, MatNativeDateModule,
         MAT_DIALOG_DATA, MAT_SNACK_BAR_DATA, MAT_RIPPLE_GLOBAL_OPTIONS  } from '@angular/material';

// Internal Permutr Modules ***********************************************************************
import { AppRoutingModule } from './app-routing.module';

// Permutr NGRX Reducers **************************************************************************
import {  permutrReducers, metaReducers, PermutrReducersToken } from './store/reducers/permutr.reducers';

// Permutr NGRX Effects ***************************************************************************
import { permutrEffects } from './store/effects/permutr.effects';

// Permutr Services *******************************************************************************
import { InventoryServiceToken } from './services/inventory/inventory-service.token';
import { inventoryServiceFactory } from './factories/inventory-service.factory';

import { EchoServiceToken } from './services/echo/echo-service.token';
import { echoServiceFactory } from './factories/echo-service.factory';

import { CrossComponentServiceToken } from './services/cross-component/cross-component-service.token';
import { crossComponentServiceFactory } from './factories/cross-component-service.factory';

import { DependencyChainServiceToken } from './services/dependency-chain/dependency-chain-service.token';
import { dependencyChainServiceFactory } from './factories/dependency-chain-service.factory';

import { IsolationServiceToken } from './services/isolation/isolation-service.token';
import { isolationServiceFactory } from './factories/isolation-service.factory';

import { TriggerGetServiceToken } from './services/trigger-get/trigger-get-service.token';
import { triggerGetServiceFactory } from './factories/trigger-get-service.factory';

import { RequestResponseLoopServiceToken } from './services/request-response-loop/request-response-loop-service.token';
import { requestResponseLoopServiceFactory } from './factories/request-response-loop-service.factory';

// Permutr Components *****************************************************************************
import { PermutrBootstrapComponent } from './bootstrap/bootstrap.component';
import { PermutrShellComponent } from './shell/shell.component';
import { PermutrToolbarComponent } from './toolbar/toolbar.component';
import { PermutrToolbarNavigationComponent } from './toolbar/toolbar-navigation/toolbar-navigation.component';
import { PermutrToolbarActionsComponent } from './toolbar/toolbar-actions/toolbar-actions.component';
import { PermutrHomeContainerComponent } from './content/home/container/home-container.component';
import { PermutrChatContainerComponent } from './content/chat/container/chat-container.component';
import { PermutrMarketplaceContainerComponent } from './content/marketplace/container/market-place-container.component';
import { PermutrInventoryContainerComponent } from './content/inventory/container/inventory-container.component';
import { PermutrInventoryAnalyticsComponent } from './content/inventory/analytics/inventory-analytics.component';
import { PermutrInventoryCollectionComponent } from './content/inventory/collection/inventory-collection.component';
import { PermutrInventoryHeaderComponent } from './content/inventory/header/inventory-header.component';
import { PermutrInventoryDialogComponent } from './content/inventory/dialog/inventory-dialog.component';
import { PermutrEchoContainerComponent } from './content/echo/container/echo-container.component';
import { PermutrEchoHeaderComponent } from './content/echo/header/echo-header.component';
import { PermutrJavaEchoComponent } from './content/echo/java-echo/java-echo.component';
import { PermutrPythonEchoComponent } from './content/echo/python-echo/python-echo.component';
import { PermutrCrossComponentContainerComponent } from './content/cross-component/container/cross-component-container.component';
import { PermutrCrossComponentHeaderComponent } from './content/cross-component/header/cross-component-header.component';
import { PermutrParentChildComponent } from './content/cross-component/parent-child/parent/parent-child.component';
import { PermutrChildInputComponent } from './content/cross-component/parent-child/child-input/child-input.component';
import { PermutrChildOutputComponent } from './content/cross-component/parent-child/child-output/child-output.component';
import { PermutrServiceBasedContainerComponent } from './content/cross-component/service-based/container/container.component';
import { PermutrServiceInputComponent } from './content/cross-component/service-based/service-input/service-input.component';
import { PermutrServiceOutputComponent } from './content/cross-component/service-based/service-output/service-output.component';
import { PermutrStoreBasedContainerComponent } from './content/cross-component/store-based/container/container.component';
import { PermutrStoreInputComponent } from './content/cross-component/store-based/store-input/store-input.component';
import { PermutrStoreOutputComponent } from './content/cross-component/store-based/store-output/store-output.component';
import { PermutrPatternsContainerComponent } from './content/patterns/container/container.component';
import { PermutrPatternsHeaderComponent } from './content/patterns/header/header.component';
import { PermutrDependencyChainComponent } from './content/patterns/dependency-chain/dependency-chain.component';
import { PermutrIsolationComponent } from './content/patterns/isolation/isolation.component';
import { PermutrTriggerGetComponent } from './content/patterns/trigger-get/trigger-get.component';
import { PermutrRequestResponseLoopComponent } from './content/patterns/request-response-loop/request-response.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatOptionModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSelectModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatTabsModule,
        MatTableModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatNativeDateModule,
        StoreModule.forRoot(PermutrReducersToken, { metaReducers }),
        EffectsModule.forRoot(permutrEffects),
        StoreDevtoolsModule.instrument({
            maxAge: 75
        }),
        AppRoutingModule,
    ],
    declarations: [
        PermutrBootstrapComponent,
        PermutrShellComponent,
        PermutrToolbarComponent,
        PermutrToolbarActionsComponent,
        PermutrToolbarNavigationComponent,
        PermutrChatContainerComponent,
        PermutrHomeContainerComponent,
        PermutrInventoryContainerComponent,
        PermutrMarketplaceContainerComponent,
        PermutrInventoryAnalyticsComponent,
        PermutrInventoryCollectionComponent,
        PermutrInventoryHeaderComponent,
        PermutrInventoryDialogComponent,
        PermutrEchoContainerComponent,
        PermutrEchoHeaderComponent,
        PermutrJavaEchoComponent,
        PermutrPythonEchoComponent,
        PermutrCrossComponentContainerComponent,
        PermutrCrossComponentHeaderComponent,
        PermutrParentChildComponent,
        PermutrChildInputComponent,
        PermutrChildOutputComponent,
        PermutrServiceBasedContainerComponent,
        PermutrServiceInputComponent,
        PermutrServiceOutputComponent,
        PermutrStoreBasedContainerComponent,
        PermutrStoreInputComponent,
        PermutrStoreOutputComponent,
        PermutrPatternsContainerComponent,
        PermutrPatternsHeaderComponent,
        PermutrDependencyChainComponent,
        PermutrIsolationComponent,
        PermutrTriggerGetComponent,
        PermutrRequestResponseLoopComponent
    ],
    entryComponents: [
        PermutrInventoryDialogComponent
    ],
    providers: [
        {
            provide: PermutrReducersToken,
            useValue: permutrReducers
        },
        {
            provide: InventoryServiceToken,
            useFactory: inventoryServiceFactory,
            deps: [ HttpClient ]
        },
        {
            provide: EchoServiceToken,
            useFactory: echoServiceFactory,
            deps: [ HttpClient ]
        },
        {
            provide: CrossComponentServiceToken,
            useFactory: crossComponentServiceFactory
        },
        {
            provide: DependencyChainServiceToken,
            useFactory: dependencyChainServiceFactory,
            deps: [ HttpClient ]
        },
        {
            provide: IsolationServiceToken,
            useFactory: isolationServiceFactory,
            deps: [ HttpClient ]
        },
        {
            provide: TriggerGetServiceToken,
            useFactory: triggerGetServiceFactory,
            deps: [ HttpClient ]
        },
        {
            provide: RequestResponseLoopServiceToken,
            useFactory: requestResponseLoopServiceFactory,
            deps: [ HttpClient ]
        }
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
    ],
    bootstrap: [
        PermutrBootstrapComponent
    ]
})
export class AppModule { }
