import { NavigationState } from './navigation.state';
import { InventoryState } from './inventory.state';
import { EchoState } from './echo.state';
import { CrossComponentState } from './cross-component.state';
import { PatternsState } from './patterns.state';

export interface PermutrGlobalState {
    navigation: NavigationState;
    inventory: InventoryState;
    echo: EchoState;
    crossComponent: CrossComponentState;
    patterns: PatternsState;
}
