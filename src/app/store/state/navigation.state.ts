import { NavigationOptions } from '@models/navigation/navigation-options';

export interface NavigationState {
    current: NavigationOptions;
}
