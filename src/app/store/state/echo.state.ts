import { IEcho } from '@models/echo/_echo.barrel';

export interface EchoState {
    java: IEcho[];
    python: IEcho[];
}
