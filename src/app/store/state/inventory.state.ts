import { InventoryCollection } from '@models/inventory/_inventory.barrel';

export interface InventoryState {
    filter: string;
    collection: InventoryCollection;
}
