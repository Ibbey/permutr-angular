export interface PatternsState {
    chainA: any;
    chainB: any;
    chainC: any;

    isolationA: any;
    isolationB: any;

    triggerGetA: any;
    triggerGetB: any;
    triggerGetC: any;
    triggerGetD: any;

    requests: any[];
    response: any[];
}
