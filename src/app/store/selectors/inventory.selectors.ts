import { createSelector, createFeatureSelector } from '@ngrx/store';

import { PermutrGlobalState } from '../state/permutr.state';
import { InventoryState } from '../state/inventory.state';

const getInventoryState = createFeatureSelector<PermutrGlobalState, InventoryState>('inventory');

export const getInventoryFilter = createSelector(getInventoryState, (state: InventoryState) => {
    if (state !== undefined) {
        return state.filter;
    }

    return undefined;
});

export const getInventoryCollection = createSelector(getInventoryState, (state: InventoryState) => {
    if (state !== undefined) {
        return state.collection;
    }

    return undefined;
});
