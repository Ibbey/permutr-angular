import { createSelector, createFeatureSelector } from '@ngrx/store';

import { PermutrGlobalState } from '../state/permutr.state';
import { CrossComponentState } from '../state/cross-component.state';

const getCrossComponentState = createFeatureSelector<PermutrGlobalState, CrossComponentState>('crossComponent');

export const getMessage = createSelector(getCrossComponentState, (state: CrossComponentState) => {
    if (state !== undefined) {
        return state.message;
    }

    return undefined;
});
