import { createSelector, createFeatureSelector } from '@ngrx/store';

import { PermutrGlobalState } from '../state/permutr.state';
import { EchoState } from '../state/echo.state';

const getEchoState = createFeatureSelector<PermutrGlobalState, EchoState>('echo');

export const getJava = createSelector(getEchoState, (state: EchoState) => {
    if (state !== undefined) {
        return state.java;
    }

    return undefined;
});

export const getPython = createSelector(getEchoState, (state: EchoState) => {
    if (state !== undefined) {
        return state.python;
    }

    return undefined;
});
