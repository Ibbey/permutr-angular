import { createSelector, createFeatureSelector } from '@ngrx/store';

import { PermutrGlobalState } from '../state/permutr.state';
import { PatternsState } from '../state/patterns.state';

const getPatternsState = createFeatureSelector<PermutrGlobalState, PatternsState>('patterns');

export const getChainA = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.chainA;
    }

    return undefined;
});

export const getChainB = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.chainB;
    }

    return undefined;
});

export const getChainC = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.chainC;
    }

    return undefined;
});

export const getIsolationA = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.isolationA;
    }

    return undefined;
});

export const getIsolationB = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.isolationB;
    }

    return undefined;
});

export const getTriggerGetA = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.triggerGetA;
    }

    return undefined;
});

export const getTriggerGetB = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.triggerGetB;
    }

    return undefined;
});

export const getTriggerGetC = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.triggerGetC;
    }

    return undefined;
});

export const getTriggerGetD = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.triggerGetD;
    }

    return undefined;
});

export const getRequests = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.requests;
    }

    return undefined;
});

export const getResponse = createSelector(getPatternsState, (state: PatternsState) => {
    if (state !== undefined) {
        return state.response;
    }

    return undefined;
});
