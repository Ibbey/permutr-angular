import { createSelector, createFeatureSelector } from '@ngrx/store';

import { NavigationOptions } from '@models/navigation/navigation-options';

import { NavigationState } from '../state/navigation.state';
import { PermutrGlobalState } from '../state/permutr.state';

const getNavigationState = createFeatureSelector<PermutrGlobalState, NavigationState>('navigation');

export const getCurrentNavigation = createSelector(getNavigationState, (state: NavigationState) => {
    if (state !== undefined) {
        return state.current;
    }

    return NavigationOptions.HOME;
});

