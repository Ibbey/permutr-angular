import { Injectable, Inject } from '@angular/core';
import { Observable, of, merge } from 'rxjs';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { PermutrGlobalState } from '../state/permutr.state';
import * as ACTIONS from '../actions/patterns.actions';

import { IDependencyChainService } from '@services/dependency-chain/dependency-chain-service.contract';
import { DependencyChainServiceToken } from '@services/dependency-chain/dependency-chain-service.token';

import { IIsolationService } from '@services/isolation/isolation-service.contract';
import { IsolationServiceToken } from '@services/isolation/isolation-service.token';

import { ITriggerGetService } from '@services/trigger-get/trigger-get-service.contract';
import { TriggerGetServiceToken } from '@services/trigger-get/trigger-get-service.token';

import { IRequestResponseLoopService } from '@services/request-response-loop/request-response-loop-service.contract';
import { RequestResponseLoopServiceToken } from '@services/request-response-loop/request-response-loop-service.token';

@Injectable()
export class PatternsEffects {
    // Member Variables ***************************************************************************
    private readonly _actions$: Actions;
    private readonly _store: Store<PermutrGlobalState>;
    private readonly _chainService: IDependencyChainService;
    private readonly _isolationService: IIsolationService;
    private readonly _triggerGetService: ITriggerGetService;
    private readonly _requestResponseService: IRequestResponseLoopService;

    // Effects ************************************************************************************
    @Effect() triggerDependencyChain$: Observable<Action>;
    @Effect() chainDependencyA$: Observable<Action>;
    @Effect() chainDependencyB$: Observable<Action>;
    @Effect() chainDepedencyC$: Observable<Action>;
    @Effect() triggerIsolation$: Observable<Action>;
    @Effect() isolationA$: Observable<Action>;
    @Effect() isolationB$: Observable<Action>;
    @Effect() triggerGet$: Observable<Action>;
    @Effect() tgValueA$: Observable<Action>;
    @Effect() tgValueB$: Observable<Action>;
    @Effect() tgValueC$: Observable<Action>;
    @Effect() tgValueD$: Observable<Action>;
    @Effect() triggerResponseRequest$: Observable<Action>;
    @Effect() triggerGetRequestQueue$: Observable<Action>;
    @Effect() responseContentEffect$: Observable<Action>;

    // Constructor ********************************************************************************
    constructor (actions$: Actions, store: Store<PermutrGlobalState>,
                 @Inject(DependencyChainServiceToken) chainService: IDependencyChainService,
                 @Inject(IsolationServiceToken) isolationService: IIsolationService,
                 @Inject(TriggerGetServiceToken) triggerGetService: ITriggerGetService,
                 @Inject(RequestResponseLoopServiceToken) requestResponService: IRequestResponseLoopService) {
        this._actions$ = actions$;
        this._store = store;
        this._chainService = chainService;
        this._isolationService = isolationService;
        this._triggerGetService = triggerGetService;
        this._requestResponseService = requestResponService;

        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this.triggerDependencyChain$ = this.getDependencyChainTriggerEffect();
        this.chainDependencyA$ = this.getChainDependencyAEffect();
        this.chainDependencyB$ = this.getChainDependencyBEffect();
        this.chainDepedencyC$ = this.getChainDependencyCEffect();

        this.triggerIsolation$ = this.getIsolationTriggerEffect();
        this.isolationA$ = this.getIsolationAEffect();
        this.isolationB$ = this.getIsolationBEffect();

        this.triggerGet$ = this.getTriggerGetEffect();
        this.tgValueA$ = this.getTGValueAEffect();
        this.tgValueB$ = this.getTGValueBEffect();
        this.tgValueC$ = this.getTGValueCEffect();
        this.tgValueD$ = this.getTGValueDEffect();

        this.triggerResponseRequest$ = this.getTriggerRequestResponseEffect();
        this.triggerGetRequestQueue$ = this.getTriggerRequestQueueEffect();
        this.responseContentEffect$ = this.getResponseContentEffect();
    }

    // Trigger Effects ****************************************************************************
    private getDependencyChainTriggerEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.INIT_CHAIN_DEPENDENCY),
            switchMap(() => {
                return of(new ACTIONS.GetDependencyAAction());
            })
        );
    }

    private getIsolationTriggerEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.INIT_ISOLATION),
            switchMap(() => {
                return merge([
                    new ACTIONS.GetIsolationAAction(),
                    new ACTIONS.GetIsolationBAction()
                ]);
            })
        );
    }

    private getTriggerGetEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.INIT_TRIGGER_GET),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const payload = (action as ACTIONS.InitTriggerGetAction).payload;
                const actions = this.getTriggerGetActions(payload);
                return merge(actions);
            })
        );
    }

    private getTriggerRequestResponseEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.INIT_REQUEST_RESPONSE_LOOP),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const payload = (action as ACTIONS.InitRequestResponseLoopAction).payload;
                return of(new ACTIONS.SetRequestQueueAction(payload));
            })
        );
    }

    private getTriggerRequestQueueEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.SET_REQUEST_QUEUE, ACTIONS.REMOVE_REQUEST_ITEM),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const queue = state.patterns.requests;
                if (queue.length !== 0) {
                    const item = queue[0];
                    return of(new ACTIONS.GetResponseContentAction(item));
                }

                return merge([]);
            })
        );
    }

    // Reaction Effects ***************************************************************************
    private getChainDependencyAEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_DEPENDENCY_A),
            switchMap(() => {
                return this._chainService.getDependencyA().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetDependencyAAction(response));
                    })
                );
            })
        );
    }

    private getChainDependencyBEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.SET_DEPENDENCY_A),
            switchMap(() => {
                return this._chainService.getDependencyB().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetDependencyBAction(response));
                    })
                );
            })
        );
    }

    private getChainDependencyCEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.SET_DEPENDENCY_B),
            switchMap(() => {
                return this._chainService.getDependencyC().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetDependencyCAction(response));
                    })
                );
            })
        );
    }

    private getIsolationAEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_ISOLATION_A),
            switchMap(() => {
                return this._isolationService.getIsolationA().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetIsolationAAction(response));
                    })
                );
            })
        );
    }

    private getIsolationBEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_ISOLATION_B),
            switchMap(() => {
                return this._isolationService.getIsolationB().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetIsolationBAction(response));
                    })
                );
            })
        );
    }

    private getTGValueAEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_TRIGGER_GET_A),
            switchMap(() => {
                return this._triggerGetService.getValueA().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetTriggerGetAAction(response));
                    })
                );
            })
        );
    }

    private getTGValueBEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_TRIGGER_GET_B),
            switchMap(() => {
                return this._triggerGetService.getValueB().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetTriggerGetBAction(response));
                    })
                );
            })
        );
    }

    private getTGValueCEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_TRIGGER_GET_C),
            switchMap(() => {
                return this._triggerGetService.getValueC().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetTriggerGetCAction(response));
                    })
                );
            })
        );
    }

    private getTGValueDEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_TRIGGER_GET_D),
            switchMap(() => {
                return this._triggerGetService.getValueD().pipe(
                    switchMap((response: any) => {
                        return of(new ACTIONS.SetTriggerGetDAction(response));
                    })
                );
            })
        );
    }

    private getResponseContentEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ACTIONS.GET_RESPONSE_CONTENT),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const payload = (action as ACTIONS.GetResponseContentAction).payload;
                return this._requestResponseService.getResponse(payload).pipe(
                    switchMap((response: any) => {
                        return merge([
                            new ACTIONS.SetResponseContentAction(response),
                            new ACTIONS.RemoveRequestItemAction(payload)
                        ]);
                    })
                );
            })
        );
    }
    // Helper Methods *****************************************************************************
    private getTriggerGetActions(payload: boolean): Action[] {
        if (payload === true) {
            return [
                new ACTIONS.GetTriggerGetAAction(),
                new ACTIONS.GetTriggerGetCAction()
            ];
        }

        return [
            new ACTIONS.GetTriggerGetBAction(),
            new ACTIONS.GetTriggerGetDAction()
        ];
    }
}
