import { Injectable, Inject } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { IEchoService } from '@services/echo/echo-service.contract';
import { EchoServiceToken } from '@services/echo/echo-service.token';

import { IEcho } from '@models/echo/_echo.barrel';

import { PermutrGlobalState } from '../state/permutr.state';

import * as ECHO_ACTIONS from '../actions/echo.actions';
import { EMPTY_ARRAY } from '@angular/core/src/render3/definition';

@Injectable()
export class EchoEffects {
    // Member Variables ***************************************************************************
    private _actions$: Actions;
    private _store: Store<PermutrGlobalState>;
    private _service: IEchoService;

    // Effects ************************************************************************************
    @Effect() javaEchoTriggerEffect$: Observable<Action>;
    @Effect() pythonEchoTriggerEffect$: Observable<Action>;

    // Constructor ********************************************************************************
    constructor (actions$: Actions, store: Store<PermutrGlobalState>,
                 @Inject(EchoServiceToken) service: IEchoService) {
        this._actions$ = actions$;
        this._store = store;
        this._service = service;

        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this.javaEchoTriggerEffect$ = this.getJavaTriggerEffect();
        this.pythonEchoTriggerEffect$ = this.getPythonTriggerEffect();
    }

    private getJavaTriggerEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ECHO_ACTIONS.REQUEST_JAVA_ECHO),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const message = (action as ECHO_ACTIONS.RequestJavaEchoAction).payload;
                return this._service.pingJava(message).pipe(
                    switchMap((response: IEcho) => {
                        return of(new ECHO_ACTIONS.SetJavaEchoAction(response));
                    }),
                    catchError(error => {
                        console.error(error);
                        return EMPTY;
                    })
                );
            }),
            catchError<any, Action>(error => {
                console.error(error);
                return EMPTY;
            })
        );
    }

    private getPythonTriggerEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(ECHO_ACTIONS.REQUEST_PYTHON_ECHO),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const message = (action as ECHO_ACTIONS.RequestPythonEchoAction).payload;
                return this._service.pingPython(message).pipe(
                    switchMap((response: IEcho) => {
                        return of(new ECHO_ACTIONS.SetPythonEchoAction(response));
                    }),
                    catchError(error => {
                        console.error(error);
                        return of(undefined);
                    })
                );
            }),
            catchError<any, Action>(error => {
                console.error(error);
                return of(undefined);
            })
        );
    }
}

