import { Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { PermutrGlobalState } from '../state/permutr.state';

import { InventoryCollection } from '@models/inventory/_inventory.barrel';

import { IInventoryService } from '@services/inventory/inventory-service.contract';
import { InventoryServiceToken } from '@services/inventory/inventory-service.token';

import * as INV_ACTIONS from '../actions/inventory.actions';


@Injectable()
export class InventoryEffects {
    // Member Variables ***************************************************************************
    private readonly _actions$: Actions;
    private readonly _store: Store<PermutrGlobalState>;
    private readonly _service: IInventoryService;

    // Effects ************************************************************************************
    @Effect() inventoryFilterEffect$: Observable<Action>;
    @Effect() inventoryCollectionEffect$: Observable<Action>;

    // Constructor ********************************************************************************
    constructor(actions$: Actions, store: Store<PermutrGlobalState>, @Inject(InventoryServiceToken) service) {
        this._actions$ = actions$;
        this._store = store;
        this._service = service;

        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this.inventoryFilterEffect$ = this.getInventoryFilterEffect();
        this.inventoryCollectionEffect$ = this.getInventoryCollectionEffect();
    }

    private getInventoryFilterEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(INV_ACTIONS.SET_INVENTORY_FILTER),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const filter = (action as INV_ACTIONS.SetInventoryFilterAction).payload;
                return of(new INV_ACTIONS.GetInventoryCollectionAction(filter));
            })
        );
    }

    private getInventoryCollectionEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(INV_ACTIONS.GET_INVENTORY_COLLECTION),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const filter = (action as INV_ACTIONS.GetInventoryCollectionAction).payload;
                return this._service.getCollection(filter).pipe(
                    switchMap((response: InventoryCollection) => {
                        return of(new INV_ACTIONS.SetInventoryCollectionAction(response));
                    })
                );
            })
        );
    }
}
