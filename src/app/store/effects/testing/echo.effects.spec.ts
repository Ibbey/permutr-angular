import { TestBed } from '@angular/core/testing';
import { Observable, EMPTY } from 'rxjs';
import { Action, StoreModule } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jasmine-marbles';

import { IEcho } from '@models/echo/echo';

import { EchoService } from '@services/echo/echo-service.service';
import { EchoServiceToken } from '@services/echo/echo-service.token';

import { EchoEffects } from '@store/effects/echo.effects';
import * as ACTIONS from '@store/actions/echo.actions';

describe('EchoEffects', () => {

    let actions: Observable<Action>;
    let effects: EchoEffects;
    let service: EchoService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                StoreModule.forRoot(() => jest.fn())
            ],
            providers: [
                EchoEffects,
                provideMockActions(() => actions),
                {
                    provide: EchoServiceToken,
                    useValue: EchoService
                }
            ]
        });

        effects = TestBed.get(EchoEffects);
        service = TestBed.get(EchoServiceToken);
    });

    it('should be created properly', () => {

        expect(effects).toBeDefined();

    });

    describe('javaEchoTriggerEffect$', () => {
        it('should trigger a java echo request properly', () => {

            const message = 'test';
            const echo: IEcho = <IEcho>{
                id: 1,
                message: 'message',
                source: 'source',
                timestamp: new Date()
            };
            const action = new ACTIONS.RequestJavaEchoAction(message);
            const outcome = new ACTIONS.SetJavaEchoAction(echo);

            actions = hot('-a-', { a: action });

            const response = cold('-a|', { a: echo });
            const expected = cold('--b', { b: outcome });

            service.pingJava = jest.fn(() => response);

            expect(effects.javaEchoTriggerEffect$).toBeObservable(expected);

        });

        // it('on failure, it should return an EMPTY observable properly', () => {

        //     const message = 'test';
        //     const action = new ACTIONS.RequestJavaEchoAction(message);
        //     const error = new Error('error has occurred');
        //     const outcome = EMPTY;

        //     actions = hot('-a-', { a: action });

        //     const response = cold('-#|', {}, error);
        //     const expected = cold('--(b|)', { b: outcome });

        //     service.pingJava = jest.fn(() => response);

        //     expect(effects.javaEchoTriggerEffect$).toBeObservable(expected);

        // });

    });

    describe('pythonEchoTriggerEffect$', () => {
        it('should trigger a python echo request properly', () => {

            const message = 'test';
            const echo: IEcho = <IEcho>{
                id: 1,
                message: 'message',
                source: 'source',
                timestamp: new Date()
            };
            const action = new ACTIONS.RequestPythonEchoAction(message);
            const outcome = new ACTIONS.SetPythonEchoAction(echo);

            actions = hot('-a-', { a: action });

            const response = cold('-a|', { a: echo });
            const expected = cold('--b', { b: outcome });

            service.pingPython = jest.fn(() => response);

            expect(effects.pythonEchoTriggerEffect$).toBeObservable(expected);

        });
    });

});
