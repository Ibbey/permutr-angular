import { InventoryEffects } from './inventory.effects';
import { EchoEffects } from './echo.effects';
import { PatternsEffects } from './patterns.effects';

export const permutrEffects = [
    InventoryEffects,
    EchoEffects,
    PatternsEffects
];
