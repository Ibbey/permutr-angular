import { Action } from '@ngrx/store';

export const INIT_CHAIN_DEPENDENCY = '[PERMUTR PATTERNS] initiate the dependency chain pattern';

export const GET_DEPENDENCY_A = '[PERMUTR PATTERNS] get the dependency value for A';
export const SET_DEPENDENCY_A = '[PERMUTR PATTERNS] set the dependency value for A';
export const GET_DEPENDENCY_B = '[PERMUTR PATTERNS] get the dependency value for B';
export const SET_DEPENDENCY_B = '[PERMUTR PATTERNS] set the dependency value for B';
export const GET_DEPENDENCY_C = '[PERMUTR PATTERNS] get the dependency value for C';
export const SET_DEPENDENCY_C = '[PERMUTR PATTERNS] set the dependency value for C';

export const INIT_ISOLATION = '[PERMUTR PATTERNS] initiate the isolation (fire-and-forget) pattern';
export const GET_ISOLATION_A = '[PERMUTR PATTERNS] get the isolation value for A';
export const GET_ISOLATION_B = '[PERMUTR PATTERNS] get the isolation value for B';
export const SET_ISOLATION_A = '[PERMUTR PATTERNS] set the isolation value for A';
export const SET_ISOLATION_B = '[PERMUTR PATTERNS] set the isolation value for B';

export const INIT_TRIGGER_GET = '[PERMUTR PATTERNS] initiate the trigger get pattern';
export const GET_TRIGGER_GET_A = '[PERMUTR PATTERNS] get the trigger get value for A';
export const GET_TRIGGER_GET_B = '[PERMUTR PATTERNS] get the trigger get value for B';
export const GET_TRIGGER_GET_C = '[PERMUTR PATTERNS] get the trigger get value for C';
export const GET_TRIGGER_GET_D = '[PERMUTR PATTERNS] get the trigger get value for D';
export const SET_TRIGGER_GET_A = '[PERMUTR PATTERNS] set the trigger get value for A';
export const SET_TRIGGER_GET_B = '[PERMUTR PATTERNS] set the trigger get value for B';
export const SET_TRIGGER_GET_C = '[PERMUTR PATTERNS] set the trigger get value for C';
export const SET_TRIGGER_GET_D = '[PERMUTR PATTERNS] set the trigger get value for D';

export const INIT_REQUEST_RESPONSE_LOOP = '[PERMUTR PATTERNS] initiate the request-response-loop pattern';
export const SET_REQUEST_QUEUE = '[PERMUTR PATTERNS] set the request queue';
export const REMOVE_REQUEST_ITEM = '[PERMUTR PATTERNS] remove a request for the queue';
export const GET_RESPONSE_CONTENT = '[PERMUTR PATTERNS] get the response content';
export const SET_RESPONSE_CONTENT = '[PERMUTR PATTERNS] set the response content';

export const CLEAR_ALL = '[PERMUTR PATTERNS] clear the patterns state';

export class InitChainDependencyAction implements Action {
    public readonly type = INIT_CHAIN_DEPENDENCY;
}

export class GetDependencyAAction implements Action {
    public readonly type = GET_DEPENDENCY_A;
}

export class SetDependencyAAction implements Action {
    public readonly type = SET_DEPENDENCY_A;
    constructor (public readonly payload: any) {}
}

export class GetDependencyBAction implements Action {
    public readonly type = GET_DEPENDENCY_B;
}

export class SetDependencyBAction implements Action {
    public readonly type = SET_DEPENDENCY_B;
    constructor (public readonly payload: any) {}
}

export class GetDependencyCAction implements Action {
    public readonly type = GET_DEPENDENCY_C;
}

export class SetDependencyCAction implements Action {
    public readonly type = SET_DEPENDENCY_C;
    constructor (public readonly payload: any) {}
}

export class InitIsolationAction implements Action {
    public readonly type = INIT_ISOLATION;
}

export class GetIsolationAAction implements Action {
    public readonly type = GET_ISOLATION_A;
}

export class GetIsolationBAction implements Action {
    public readonly type = GET_ISOLATION_B;
}

export class SetIsolationAAction implements Action {
    public readonly type = SET_ISOLATION_A;
    constructor (public readonly payload: any) {}
}

export class SetIsolationBAction implements Action {
    public readonly type = SET_ISOLATION_B;
    constructor (public readonly payload: any) {}
}

export class InitTriggerGetAction implements Action {
    public readonly type = INIT_TRIGGER_GET;
    constructor (public readonly payload: boolean) {}
}

export class GetTriggerGetAAction implements Action {
    public readonly type = GET_TRIGGER_GET_A;
}

export class GetTriggerGetBAction implements Action {
    public readonly type = GET_TRIGGER_GET_B;
}
export class GetTriggerGetCAction implements Action {
    public readonly type = GET_TRIGGER_GET_C;
}
export class GetTriggerGetDAction implements Action {
    public readonly type = GET_TRIGGER_GET_D;
}

export class SetTriggerGetAAction implements Action {
    public readonly type = SET_TRIGGER_GET_A;
    constructor (public readonly payload: any) {}
}

export class SetTriggerGetBAction implements Action {
    public readonly type = SET_TRIGGER_GET_B;
    constructor (public readonly payload: any) {}
}

export class SetTriggerGetCAction implements Action {
    public readonly type = SET_TRIGGER_GET_C;
    constructor (public readonly payload: any) {}
}

export class SetTriggerGetDAction implements Action {
    public readonly type = SET_TRIGGER_GET_D;
    constructor (public readonly payload: any) {}
}

export class InitRequestResponseLoopAction implements Action {
    public readonly type = INIT_REQUEST_RESPONSE_LOOP;
    constructor (public readonly payload: any[]) {}
}

export class SetRequestQueueAction implements Action {
    public readonly type = SET_REQUEST_QUEUE;
    constructor (public readonly payload: any[]) {}
}

export class RemoveRequestItemAction implements Action {
    public readonly type = REMOVE_REQUEST_ITEM;
    constructor (public readonly payload: any) {}
}

export class GetResponseContentAction implements Action {
    public readonly type = GET_RESPONSE_CONTENT;
    constructor (public readonly payload: any) {}
}

export class SetResponseContentAction implements Action {
    public readonly type = SET_RESPONSE_CONTENT;
    constructor (public readonly payload: any) {}
}

export class ClearAllAction implements Action {
    public readonly type = CLEAR_ALL;
}

export type All =
    InitChainDependencyAction |
    GetDependencyAAction |
    SetDependencyAAction |
    GetDependencyBAction |
    SetDependencyBAction |
    GetDependencyCAction |
    SetDependencyCAction |
    InitIsolationAction |
    GetIsolationAAction |
    GetIsolationBAction |
    SetIsolationAAction |
    SetIsolationBAction |
    InitTriggerGetAction |
    GetTriggerGetAAction |
    GetTriggerGetBAction |
    GetTriggerGetCAction |
    GetTriggerGetDAction |
    SetTriggerGetAAction |
    SetTriggerGetBAction |
    SetTriggerGetCAction |
    SetTriggerGetDAction |
    InitRequestResponseLoopAction |
    SetRequestQueueAction |
    RemoveRequestItemAction |
    GetResponseContentAction |
    SetResponseContentAction |
    ClearAllAction;
