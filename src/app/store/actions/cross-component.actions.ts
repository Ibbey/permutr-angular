import { Action } from '@ngrx/store';

export const SET_MESSAGE = '[PERMUTR CROSS COMPONENT] set the message content';
export const CLEAR_MESSAGE = '[PERMUTR CROSS COMPONENT] clear the message content';

export class SetMessageAction implements Action {
    public readonly type = SET_MESSAGE;
    constructor (public readonly payload: string) {}
}

export class ClearMessageAction implements Action {
    public readonly type = CLEAR_MESSAGE;
}

export type All =
    SetMessageAction |
    ClearMessageAction;
