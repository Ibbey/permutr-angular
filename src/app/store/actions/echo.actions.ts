import { Action } from '@ngrx/store';

import { IEcho } from '@models/echo/_echo.barrel';

export const REQUEST_JAVA_ECHO = '[Permutr Echo] request an echo message from the java container';
export const SET_JAVA_ECHO = '[Permutr Echo] set the echo response message from the java container';

export const REQUEST_PYTHON_ECHO = '[Permutr Echo] request an echo message from the python container';
export const SET_PYTHON_ECHO = '[Permutr Echo] set the echo response message from the python container';

export const CLEAR_ALL_ECHO = '[Permutr Echo] clear all echo messages from both sources';

export class RequestJavaEchoAction implements Action {
    public readonly type = REQUEST_JAVA_ECHO;
    constructor (public readonly payload: string) {}
}

export class SetJavaEchoAction implements Action {
    public readonly type = SET_JAVA_ECHO;
    constructor (public readonly payload: IEcho) {}
}

export class RequestPythonEchoAction implements Action {
    public readonly type = REQUEST_PYTHON_ECHO;
    constructor (public readonly payload: string) {}
}

export class SetPythonEchoAction implements Action {
    public readonly type = SET_PYTHON_ECHO;
    constructor (public readonly payload: IEcho) {}
}

export class ClearAllEchoAction implements Action {
    public readonly type = CLEAR_ALL_ECHO;
}

export type All =
    RequestJavaEchoAction |
    SetJavaEchoAction |
    RequestPythonEchoAction |
    SetPythonEchoAction |
    ClearAllEchoAction;
