import { Action } from '@ngrx/store';

import { NavigationOptions } from 'src/app/models/navigation/navigation-options';

export const NAVIGATE_TO = '[PERMUTR NAVIGATION] navigate to the desired component';

export class NavigateToAction implements Action {
    public readonly type = NAVIGATE_TO;
    constructor(public readonly payload: NavigationOptions) {}
}

export type All =
    NavigateToAction;

