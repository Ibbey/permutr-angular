import { Action } from '@ngrx/store';

import { InventoryCollection, InventoryItem } from '@models/inventory/_inventory.barrel';

export const SELECT_INVENTORY_FILTER = '[PERMUTR INVENTORY] select the filter for the desired inventory collection';
export const SET_INVENTORY_FILTER = '[PERMUTR INVENTORY] set the filter for the desired inventory collection';

export const GET_INVENTORY_COLLECTION = '[PERMUTR INVENTORY] get the inventory collection for the provided filter';
export const SET_INVENTORY_COLLECTION = '[PERMUTR INVENTORY] set the inventory collection for the provided filter';

export const REQUEST_ITEM_ADD = '[PERMUTR INVENTORY] attempt to add the provided item to the inventory collection';
export const ADD_ITEM_SUCCESS = '[PERMUTR INVENTORY] the item was successfully added to the inventory collection';

export const REQUEST_ITEM_REMOVE = '[PERMUTR INVENTORY] attempt to remove the provided item from the inventory collection';
export const REMOVE_ITEM_SUCCESS = '[PERMUTR INVENTORY] the item was successfully removed from the inventory collection';

export const CLEAR_INVENTORY = '[PERMUTR INVENTORY] clear the inventory state';

export class SelectInventoryFilterAction implements Action {
    public readonly type = SELECT_INVENTORY_FILTER;
    constructor (public readonly payload: string) {}
}

export class SetInventoryFilterAction implements Action {
    public readonly type = SET_INVENTORY_FILTER;
    constructor (public readonly payload: string) {}
}

export class GetInventoryCollectionAction implements Action {
    public readonly type = GET_INVENTORY_COLLECTION;
    constructor (public readonly payload: string) {}
}

export class SetInventoryCollectionAction implements Action {
    public readonly type = SET_INVENTORY_COLLECTION;
    constructor (public readonly payload: InventoryCollection) {}
}

export class RequestInventoryItemAddAction implements Action {
    public readonly type = REQUEST_ITEM_ADD;
    constructor (public readonly payload: InventoryItem) {}
}

export class InventoryItemAddSuccessAction implements Action {
    public readonly type = ADD_ITEM_SUCCESS;
}

export class RequestInventoryItemRemoveAction implements Action {
    public readonly type = REQUEST_ITEM_REMOVE;
    constructor (public readonly payload: InventoryItem) {}
}

export class InventoryItemRemoveSuccessAction implements Action {
    public readonly type = REMOVE_ITEM_SUCCESS;
}

export class ClearInventoryAction implements Action {
    public readonly type = CLEAR_INVENTORY;
}

export type All =
    SelectInventoryFilterAction |
    SetInventoryFilterAction |
    GetInventoryCollectionAction |
    SetInventoryCollectionAction |
    RequestInventoryItemAddAction |
    InventoryItemAddSuccessAction |
    RequestInventoryItemRemoveAction |
    InventoryItemRemoveSuccessAction |
    ClearInventoryAction;
