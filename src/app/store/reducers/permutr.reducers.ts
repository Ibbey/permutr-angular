import { InjectionToken } from '@angular/core';
import { ActionReducerMap, MetaReducer, Action } from '@ngrx/store';

import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '@env/environment';
import { ngrxLogger } from '@common/logging/ngrx-logger';

import { PermutrGlobalState } from '../state/permutr.state';

import * as fromNavigation from './navigation.reducer';
import * as fromInventory from './inventory.reducer';
import * as fromEcho from './echo.reducer';
import * as fromCrossComponent from './cross-component.reducer';
import * as fromPatterns from './patterns.reducer';


export const permutrReducers: ActionReducerMap<PermutrGlobalState> = {
    navigation: fromNavigation.reducer,
    inventory: fromInventory.reducer,
    echo: fromEcho.reducer,
    crossComponent: fromCrossComponent.reducer,
    patterns: fromPatterns.reducer
};

export const metaReducers: MetaReducer<PermutrGlobalState>[] = !environment.production ? [ storeFreeze, ngrxLogger ] : [];

export const PermutrReducersToken = new InjectionToken<ActionReducerMap<PermutrGlobalState, Action>>('PermutrReducersToken');
