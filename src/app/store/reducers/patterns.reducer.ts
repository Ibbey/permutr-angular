import { PatternsState } from '../state/patterns.state';

import * as ACTIONS from '../actions/patterns.actions';

import { pureCollectionAdd } from '@common/common.functions';

export const INITIAL_STATE: PatternsState = {
    chainA: undefined,
    chainB: undefined,
    chainC: undefined,
    isolationA: undefined,
    isolationB: undefined,
    triggerGetA: undefined,
    triggerGetB: undefined,
    triggerGetC: undefined,
    triggerGetD: undefined,
    requests: [],
    response: undefined,
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): PatternsState {
    switch (action.type) {
        case ACTIONS.SET_DEPENDENCY_A:
            return Object.assign({}, state, {
                chainA: (action as ACTIONS.SetDependencyAAction).payload
            });
        case ACTIONS.SET_DEPENDENCY_B:
            return Object.assign({}, state, {
                chainB: (action as ACTIONS.SetDependencyBAction).payload
            });
        case ACTIONS.SET_DEPENDENCY_C:
            return Object.assign({}, state, {
                chainC: (action as ACTIONS.SetDependencyCAction).payload
            });
        case ACTIONS.SET_ISOLATION_A:
            return Object.assign({}, state, {
                isolationA: (action as ACTIONS.SetIsolationAAction).payload
            });
        case ACTIONS.SET_ISOLATION_B:
            return Object.assign({}, state, {
                isolationB: (action as ACTIONS.SetIsolationBAction).payload
            });
        case ACTIONS.SET_TRIGGER_GET_A:
            return Object.assign({}, state, {
                triggerGetA: (action as ACTIONS.SetTriggerGetAAction).payload
            });
        case ACTIONS.SET_TRIGGER_GET_B:
            return Object.assign({}, state, {
                triggerGetB: (action as ACTIONS.SetTriggerGetBAction).payload
            });
        case ACTIONS.SET_TRIGGER_GET_C:
            return Object.assign({}, state, {
                triggerGetC: (action as ACTIONS.SetTriggerGetCAction).payload
            });
        case ACTIONS.SET_TRIGGER_GET_D:
            return Object.assign({}, state, {
                triggerGetD: (action as ACTIONS.SetTriggerGetDAction).payload
            });
        case ACTIONS.SET_REQUEST_QUEUE:
            return Object.assign({}, state, {
                requests: (action as ACTIONS.SetRequestQueueAction).payload
            });
        case ACTIONS.REMOVE_REQUEST_ITEM:
            const item = (action as ACTIONS.RemoveRequestItemAction).payload;
            const list = state.requests.filter(x => x.id !== item.id);
            return Object.assign({}, state, {
                requests: list
            });
        case ACTIONS.SET_RESPONSE_CONTENT:
            const content = (action as ACTIONS.SetResponseContentAction).payload;
            return Object.assign({}, state, {
                response: pureCollectionAdd(content, state.response)
            });
        case ACTIONS.CLEAR_ALL:
            return Object.assign({}, state, {
                chainA: undefined,
                chainB: undefined,
                chainC: undefined,
                isolationA: undefined,
                isolationB: undefined,
                triggerGetA: undefined,
                triggerGetB: undefined,
                triggerGetC: undefined,
                triggerGetD: undefined,
                requests: [],
                response: undefined,
            });
        default:
            return state;
    }
}
