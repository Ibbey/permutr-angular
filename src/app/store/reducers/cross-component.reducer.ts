import { CrossComponentState } from '../state/cross-component.state';

import * as ACTIONS from '../actions/cross-component.actions';

export const INITIAL_STATE: CrossComponentState = {
    message: undefined
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): CrossComponentState {
    switch (action.type) {
        case ACTIONS.SET_MESSAGE:
            return Object.assign({}, state, {
                message: (action as ACTIONS.SetMessageAction).payload
            });
        case ACTIONS.CLEAR_MESSAGE:
            return Object.assign({}, state, {
                message: undefined
            });
        default:
            return state;
    }
}
