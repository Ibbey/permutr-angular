import { EchoState } from '../state/echo.state';

import * as ACTIONS from '../actions/echo.actions';

import { pureCollectionAdd } from '@common/common.functions';

export const INITIAL_STATE: EchoState = {
    java: [],
    python: []
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): EchoState {
    switch (action.type) {
        case ACTIONS.SET_JAVA_ECHO:
            return Object.assign({}, state, {
                java: pureCollectionAdd((action as ACTIONS.SetJavaEchoAction).payload, state.java)
            });
        case ACTIONS.SET_PYTHON_ECHO:
            return Object.assign({}, state, {
                python: pureCollectionAdd((action as ACTIONS.SetPythonEchoAction).payload, state.python)
            });
        default:
            return state;
    }
}
