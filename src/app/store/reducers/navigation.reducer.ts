import { NavigationState } from '../state/navigation.state';

import { NavigationOptions } from '@models/navigation/navigation-options';

import * as ACTIONS from '../actions/navigation.actions';

export const INITIAL_STATE: NavigationState = {
    current: NavigationOptions.HOME
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): NavigationState {
    switch (action.type) {
        case ACTIONS.NAVIGATE_TO:
            return Object.assign({}, state, {
                current: (action as ACTIONS.NavigateToAction).payload
            });
        default:
            return state;
    }
}
