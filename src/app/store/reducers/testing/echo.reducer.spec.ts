import { IEcho } from '@models/echo/_echo.barrel';

import { INITIAL_STATE, reducer } from '@store/reducers/echo.reducer';
import * as ACTIONS from '@store/actions/echo.actions';

describe('Echo Reducer', () => {

    describe('Action: SET_JAVA_ECHO', () => {

        it('should update the java state', () => {
            const payload: IEcho = <IEcho>{
                id: 1,
                message: 'dummy_message',
                source: 'java',
                timestamp: new Date()
            };

            const action = new ACTIONS.SetJavaEchoAction(payload);

            const result = reducer(INITIAL_STATE, action);

            expect(result).toEqual({
                ...INITIAL_STATE,
                python: [],
                java: [ payload ]
            });
        });

    });

    describe('Action: SET_PYTHON_ECHO', () => {

        it('should update the python state', () => {
            const payload: IEcho = <IEcho>{
                id: 1,
                message: 'dummy_message',
                source: 'java',
                timestamp: new Date()
            };

            const action = new ACTIONS.SetPythonEchoAction(payload);

            const result = reducer(INITIAL_STATE, action);

            // expect(result).toEqual({
            //     ...INITIAL_STATE,
            //     python: [ payload ],
            //     java: []
            // });
            expect(result).toEqual(Object.assign({}, INITIAL_STATE, {
                python: [ payload ],
                java: []
            }));
        });

    });
});
