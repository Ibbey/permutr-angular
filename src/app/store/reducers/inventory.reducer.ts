import { InventoryState } from '../state/inventory.state';

import * as ACTIONS from '../actions/inventory.actions';

export const INITIAL_STATE: InventoryState = {
    filter: undefined,
    collection: undefined
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): InventoryState {
    switch (action.type) {
        case ACTIONS.SET_INVENTORY_FILTER:
            return Object.assign({}, state, {
                filter: (action as ACTIONS.SetInventoryFilterAction).payload
            });
        case ACTIONS.SET_INVENTORY_COLLECTION:
            return Object.assign({}, state, {
                collection: (action as ACTIONS.SetInventoryCollectionAction).payload
            });
        case ACTIONS.CLEAR_INVENTORY:
            return Object.assign({}, state, {
                filter: undefined,
                collection: undefined
            });
        default:
            return state;
    }
}
