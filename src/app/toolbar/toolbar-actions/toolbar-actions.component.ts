import { Component } from '@angular/core';

@Component({
    selector: 'permutr-toolbar-actions',
    templateUrl: './toolbar-actions.component.html',
    styleUrls: [ './toolbar-actions.component.scss' ]
})
export class PermutrToolbarActionsComponent {
    // Member Variables ***************************************************************************

    // Public Properties **************************************************************************

    // Constructor ********************************************************************************

    // Private Methods ****************************************************************************

    // Event Handlers *****************************************************************************
    public handleHelpClicked(event: MouseEvent): void {

    }

    public handleBugReportClicked(event: MouseEvent): void {

    }

    public handleSettingsClicked(event: MouseEvent): void {

    }
}
