import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrToolbarNavigationComponent } from './toolbar-navigation.component';
import { AppModule } from '../../app.module';

describe('PermutrToolbarNavigationComponent', () => {
    let component: PermutrToolbarNavigationComponent;
    let fixture: ComponentFixture<PermutrToolbarNavigationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrToolbarNavigationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
