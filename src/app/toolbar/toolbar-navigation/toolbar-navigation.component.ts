import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { NavigationOptions } from '@models/navigation/navigation-options';

import { PermutrGlobalState } from '@store/state/permutr.state';
import * as NAV_ACTIONS from '@store/actions/navigation.actions';

@Component({
    selector: 'permutr-toolbar-navigation',
    templateUrl: './toolbar-navigation.component.html',
    styleUrls: [ './toolbar-navigation.component.scss' ]
})
export class PermutrToolbarNavigationComponent {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    // Public Properties **************************************************************************

    // Constructor ********************************************************************************
    constructor (store: Store<PermutrGlobalState>) {
        this._store = store;
    }

    // Private Methods ****************************************************************************

    // Event Handlers *****************************************************************************
    public handleNavigateHomeClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.HOME));
    }

    public handleNavigateInventoryClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.INVENTORY));
    }

    public handleNavigateMarketPlaceClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.MARKETPLACE));
    }

    public handleNavigateForumClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.FORUMS));
    }

    public handleNavigateCrossComponentClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.CROSS_COMPONENT));
    }

    public handleNavigatePatternsClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.PATTERNS));
    }

    public handleNavigateChatClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.CHAT));
    }

    public handleNavigateEchoClicked(event: MouseEvent): void {
        this._store.dispatch(new NAV_ACTIONS.NavigateToAction(NavigationOptions.ECHO));
    }
}
