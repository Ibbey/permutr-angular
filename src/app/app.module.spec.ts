import { TestBed } from '@angular/core/testing';

import { AppModule } from './app.module';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrReducersToken } from '@store/reducers/permutr.reducers';
import { InventoryServiceToken } from '@services/inventory/inventory-service.token';
import { EchoServiceToken } from '@services/echo/echo-service.token';
import { CrossComponentServiceToken } from '@services/cross-component/cross-component-service.token';
import { DependencyChainServiceToken } from '@services/dependency-chain/dependency-chain-service.token';
import { IsolationServiceToken } from '@services/isolation/isolation-service.token';
import { TriggerGetServiceToken } from '@services/trigger-get/trigger-get-service.token';
import { RequestResponseLoopServiceToken } from '@services/request-response-loop/request-response-loop-service.token';

describe('AppModule', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        });
    });

    it('should provide permutrReducers properly', () => {

        expect(() => TestBed.get(PermutrReducersToken)).toBeDefined();
    });

    it('should provide InventoryService properly', () => {

        expect(() => TestBed.get(InventoryServiceToken)).toBeDefined();

    });

    it('should provide EchoService properly', () => {

        expect(() => TestBed.get(EchoServiceToken)).toBeDefined();

    });

    it('should provide CrossComponentService properly', () => {

        expect(() => TestBed.get(CrossComponentServiceToken)).toBeDefined();

    });

    it('should provide DependencyChainService properly', () => {

        expect(() => TestBed.get(DependencyChainServiceToken)).toBeDefined();

    });

    it('should provide IsolationService properly', () => {

        expect(() => TestBed.get(IsolationServiceToken)).toBeDefined();

    });

    it('should provide TriggerGetService properly', () => {

        expect(() => TestBed.get(TriggerGetServiceToken)).toBeDefined();

    });

    it('should provide RequestResponseLoopService properly', () => {

        expect(() => TestBed.get(RequestResponseLoopServiceToken)).toBeDefined();

    });

});
