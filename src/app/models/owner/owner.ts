import { OwnerInformation } from './_owner.barrel';

export class Owner {
    // Member Variables ***************************************************************************
    private _id: string;
    private _name: string;
    private _information: OwnerInformation;
    private _admin: boolean;

    // Public Properties **************************************************************************
    public get id(): string {
        return this._id;
    }

    public get name(): string {
        return this._name;
    }

    public get information(): OwnerInformation {
        return this._information;
    }

    public set information(value: OwnerInformation) {
        this._information = value;
    }

    public get admin(): boolean {
        return this._admin;
    }

    // Constructor ********************************************************************************
    constructor (instance?: Owner) {
        if (instance !== undefined) {
            this.deserialize(instance);
        }
    }

    // Private Methods ****************************************************************************
    private deserialize(instance: Owner): void {
        for (const key in Object.keys(this)) {
            if (instance.hasOwnProperty(key) === true) {
                this[key] = instance[key];
            }
        }
    }
}
