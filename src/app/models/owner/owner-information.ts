import { OwnerAddress, OwnerPhone, OwnerEmail } from './_owner.barrel';

export class OwnerInformation {
    // Member Variables ***************************************************************************
    private _id: string;
    private _address: OwnerAddress;
    private _phone: OwnerPhone[];
    private _email: OwnerEmail[];
}
