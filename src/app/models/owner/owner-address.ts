export class OwnerAddress {
    // Member Variables ***************************************************************************
    private _id: string;
    private _street1: string;
    private _street2: string;
    private _town: string;
    private _country: string;
    private _zipcode: number;
}
