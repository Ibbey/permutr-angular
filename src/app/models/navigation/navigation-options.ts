export enum NavigationOptions {
    HOME,
    INVENTORY,
    MARKETPLACE,
    FORUMS,
    CHAT,
    ECHO,
    CROSS_COMPONENT,
    PATTERNS
}
