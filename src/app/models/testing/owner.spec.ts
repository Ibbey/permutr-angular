import { Owner, OwnerInformation } from '@models/owner/_owner.barrel';

describe('Model: Owner', () => {

    let owner: Owner;

    beforeEach(() => {
        owner = new Owner();
    });

    it('should instantiate', () => {
        expect(owner).toBeTruthy();
    });

    it('should get id properly', () => {

        const spy = jest.spyOn(owner, 'id', 'get').mockReturnValue('dummy id');

        const result = owner.id;

        expect(result).toEqual('dummy id');
        expect(spy).toHaveBeenCalled();

        spy.mockRestore();

    });

    it('should get admin properly', () => {

        const spy = jest.spyOn(owner, 'admin', 'get').mockReturnValue(true);

        const result = owner.admin;

        expect(result).toEqual(true);
        expect(spy).toHaveBeenCalled();

        spy.mockRestore();

    });

    it('should get name properly', () => {

        const spy = jest.spyOn(owner, 'name', 'get').mockReturnValue('dummy_name');

        const result = owner.name;

        expect(result).toEqual('dummy_name');
        expect(spy).toHaveBeenCalled();

        spy.mockRestore();

    });

    it('should get information properly', () => {

        const mockOwnerInformation = new OwnerInformation();
        const spy = jest.spyOn(owner, 'information', 'get').mockReturnValue(mockOwnerInformation);

        const result = owner.information;

        expect(result).toEqual(mockOwnerInformation);
        expect(spy).toHaveBeenCalled();

        spy.mockRestore();
    });

    it('should set information properly', () => {

        const mockOwnerInformation = new OwnerInformation();

        const spy = jest.spyOn(owner, 'information', 'set');

        owner.information = mockOwnerInformation;

        expect(spy).toHaveBeenCalled();
        expect(owner.information).toEqual(mockOwnerInformation);

        spy.mockRestore();
    });
});


