export interface IEcho {
    id: number;
    message: string;
    source: string;
    timestamp: Date;
}
