import { InventoryItem } from './inventory-item';

export class InventoryCollection {
    // Member Variables ***************************************************************************
    private _items: InventoryItem[];

    // Public Properties **************************************************************************
    public get items(): InventoryItem[] {
        return this._items;
    }

    // Constructor ********************************************************************************
    constructor () {
        this.initialize();
    }

    // Public Methods *****************************************************************************
    public add(item: InventoryItem): boolean {
        if (this._items.includes(item) === false) {
            this._items.push(item);
            return true;
        }

        return false;
    }

    public remove(id: string): boolean {
        if (this._items !== undefined && this._items.length !== 0) {
            const index = this._items.findIndex(x => x.id === id);
            if (index !== -1) {
                this._items = this._items.splice(index, 1);
                return true;
            }
        }

        return false;
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._items = [];
    }
}
