import { Owner } from '../owner/_owner.barrel';

import { formatCurrency } from '@common/common.functions';

export enum InventoryType {
    ARTWORK,
    STATUE,
    FIGURINE,
    TOY,
    OTHER,
    UNKNOWN
}

export enum InventoryStatus {
    HELD,
    FOR_SALE,
    PENDING_SALE,
    SOLD,
    UNKNOWN
}

export function toInventoryType(value: string): InventoryType {
    if (value !== undefined) {
        switch (value.toUpperCase()) {
            case 'ARTWORK':
                return InventoryType.ARTWORK;
            case 'STATUE':
                return InventoryType.STATUE;
            case 'FIGURINE':
                return InventoryType.FIGURINE;
            case 'TOY':
                return InventoryType.TOY;
            case 'OTHER':
                return InventoryType.OTHER;
            default:
                return InventoryType.UNKNOWN;
        }
    }

    return InventoryType.UNKNOWN;
}

export function fromInventoryType(value: InventoryType): string {
    switch (value) {
        case InventoryType.ARTWORK:
            return 'Artwork';
        case InventoryType.FIGURINE:
            return 'Figurine';
        case InventoryType.OTHER:
            return 'Other';
        case InventoryType.STATUE:
            return 'Statue';
        case InventoryType.TOY:
            return 'Toy';
        case InventoryType.UNKNOWN:
        default:
            return 'Unknown';
    }
}

export function toInventoryStatus(value: string): InventoryStatus {
    if (value !== undefined) {
        switch (value.toUpperCase()) {
            case 'HELD':
                return InventoryStatus.HELD;
            case 'FOR_SALE':
            case 'FOR SALE':
            case 'FORSALE':
                return InventoryStatus.FOR_SALE;
            case 'PENDING':
            case 'PENDING SALE':
            case 'PENDING_SALE':
            case 'PENDINGSALE':
                return InventoryStatus.PENDING_SALE;
            case 'SOLD':
                return InventoryStatus.SOLD;
            default:
                return InventoryStatus.UNKNOWN;
        }
    }

    return InventoryStatus.UNKNOWN;
}

export function fromInventoryStatus(value: InventoryStatus): string {
    switch (value) {
        case InventoryStatus.FOR_SALE:
            return 'For Sale';
        case InventoryStatus.HELD:
            return 'Held';
        case InventoryStatus.PENDING_SALE:
            return 'Pending Sale';
        case InventoryStatus.SOLD:
            return 'Sold';
        case InventoryStatus.UNKNOWN:
        default:
            return 'Unknown';
    }
}

export class InventoryNote {
    // Member Variables ***************************************************************************
    private _source: Owner;
    private _note: string;
    private _timestamp: Date;
}

export interface IInventoryItem {
    ownerId: string;
    type: InventoryType;
    name: string;
    category: string;
    description: string;
    acquisition: Date;
    cost: number;
    estimatedValue: number;
    actualValue: number;
    location: string;
    status: InventoryStatus;
    public: boolean;
}

export class InventoryItem {
    // Member Variables ***************************************************************************
    private readonly _id: string = undefined;
    private _ownerId: string = undefined;
    private _type: InventoryType = undefined;
    private _name: string = undefined;
    private _category: string = undefined;
    private _description: string = undefined;
    private _acquisition: Date = undefined;
    private _cost: number = undefined;
    private _estimateValue: number = undefined;
    private _actualValue: number = undefined;
    private _location: string = undefined;
    private _status: InventoryStatus = undefined;
    private _public: boolean = undefined;

    // Public Properties **************************************************************************
    public get id(): string {
        return this._id;
    }

    public get ownerId(): string {
        return this._ownerId;
    }

    public get type(): InventoryType {
        return this._type;
    }

    public get typeFormatted(): string {
        return fromInventoryType(this._type);
    }

    public get name(): string {
        return this._name;
    }

    public get category(): string {
        return this._category;
    }

    public get description(): string {
        return this._description;
    }

    public set description(value: string) {
        this._description = value;
    }

    public get acquisition(): Date {
        return this._acquisition;
    }

    public get acquisitionFormatted(): string {
        if (this._acquisition !== undefined) {
            return this._acquisition.toLocaleDateString();
        }

        return '';
    }

    public get cost(): number {
        return this._cost;
    }

    public get costFormatted(): string {
        if (this._cost !== undefined) {
            return formatCurrency(this._cost);
        }

        return '';
    }

    public get estimatedValue(): number {
        return this._estimateValue;
    }

    public set estimatedValue(value: number) {
        this._estimateValue = value;
    }

    public get estimatedValueFormatted(): string {
        if (this._estimateValue !== undefined) {
            return formatCurrency(this._estimateValue);
        }

        return '';
    }

    public get actualValue(): number {
        return this._actualValue;
    }

    public set actualValue(value: number) {
        this._actualValue = value;
    }

    public get actualValueFormatted(): string {
        if (this._actualValue !== undefined) {
            return formatCurrency(this._actualValue);
        }

        return '';
    }

    public get location(): string {
        return this._location;
    }

    public set location(value: string) {
        this._location = value;
    }

    public get status(): InventoryStatus {
        return this._status;
    }

    public set status(value: InventoryStatus) {
        this._status = value;
    }

    public get statusFormatted(): string {
        return fromInventoryStatus(this._status);
    }

    public get public(): boolean {
        return this._public;
    }

    public set public(value: boolean) {
        this._public = value;
    }

    // Constructor ********************************************************************************
    constructor (instance?: IInventoryItem) {
        this._id = this.generateId();
        this._acquisition = new Date(instance.acquisition);
        this._actualValue = instance.actualValue;
        this._category = instance.category;
        this._cost = instance.cost;
        this._description = instance.description;
        this._estimateValue = instance.estimatedValue;
        this._location = instance.location;
        this._name = instance.name;
        this._ownerId = instance.ownerId;
        this._public = instance.public;
        this._status = toInventoryStatus(instance.status as any);
        this._type = toInventoryType(instance.type as any);
    }

    // Private Methods ****************************************************************************
    private generateId(): string {
        return Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
    }
}
