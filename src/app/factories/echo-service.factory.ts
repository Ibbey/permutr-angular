import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { IEchoService } from '@services/echo/echo-service.contract';
import { EchoService } from '@services/echo/echo-service.service';
import { LocalEchoService } from '@services/echo/echo-service.local.service';

export function echoServiceFactory(http: HttpClient): IEchoService {

    const jServiceUri = environment.jEchoUri;
    const pServiceUri = environment.pEchoUri;

    if (jServiceUri === 'local' || pServiceUri === 'local') {
        return new LocalEchoService();
    }

    return new EchoService(http, jServiceUri, pServiceUri);
}
