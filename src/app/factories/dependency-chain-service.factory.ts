import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { IDependencyChainService } from '@services/dependency-chain/dependency-chain-service.contract';
import { DependencyChainService } from '@services/dependency-chain/dependency-chain-service.service';
import { LocalDependencyChainService } from '@services/dependency-chain/dependency-chain-service.local.service';

export function dependencyChainServiceFactory(http: HttpClient): IDependencyChainService {

    const dependencyChainUri = environment.dependencyChainUri;

    if (dependencyChainUri === 'local') {
        return new LocalDependencyChainService();
    }

    return new DependencyChainService(http, dependencyChainUri);
}
