import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { ITriggerGetService } from '@services/trigger-get/trigger-get-service.contract';
import { TriggerGetService } from '@services/trigger-get/trigger-get-service.service';
import { LocalTriggerGetService } from '@services/trigger-get/trigger-get-service.local.service';

export function triggerGetServiceFactory(http: HttpClient): ITriggerGetService {

    const triggerGetUri = environment.triggerGetUri;

    if (triggerGetUri === 'local') {
        return new LocalTriggerGetService();
    }

    return new TriggerGetService(http, triggerGetUri);
}
