import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { IIsolationService } from '@services/isolation/isolation-service.contract';
import { IsolationService } from '@services/isolation/isolation-service.service';
import { LocalIsolationService } from '@services/isolation/isolation-service.local.service';

export function isolationServiceFactory(http: HttpClient): IIsolationService {

    const isolationUri = environment.isolationUri;

    if (isolationUri === 'local') {
        return new LocalIsolationService();
    }

    return new IsolationService(http, isolationUri);
}
