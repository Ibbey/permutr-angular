import { ICrossComponentService } from '@services/cross-component/cross-component-service.contract';
import { CrossComponentService } from '@services/cross-component/cross-component-service.service';

export function crossComponentServiceFactory(): ICrossComponentService {
    return new CrossComponentService();
}
