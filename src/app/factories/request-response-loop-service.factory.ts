import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { IRequestResponseLoopService } from '@services/request-response-loop/request-response-loop-service.contract';
import { RequestResponseLoopService } from '@services/request-response-loop/request-response-loop-service.service';
import { LocalRequestResponseLoopService } from '@services/request-response-loop/request-response-loop-service.local.service';

export function requestResponseLoopServiceFactory(http: HttpClient): IRequestResponseLoopService {

    const requestResponseLoopUri = environment.requestResponseLoopUri;

    if (requestResponseLoopUri === 'local') {
        return new LocalRequestResponseLoopService();
    }

    return new RequestResponseLoopService(http, requestResponseLoopUri);
}
