import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { IInventoryService } from '@services/inventory/inventory-service.contract';
import { InventoryService } from '@services/inventory/inventory-service.service';
import { LocalInventoryService } from '@services/inventory/inventory-service.local.service';

export function inventoryServiceFactory(http: HttpClient): IInventoryService {
    const serviceUri = environment.inventoryUri;

    if (serviceUri === 'local') {
        return new LocalInventoryService(http);
    }

    return new InventoryService(http, serviceUri);
}
