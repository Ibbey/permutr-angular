import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ICrossComponentService } from '@services/cross-component/cross-component-service.contract';
import { CrossComponentServiceToken } from '@services/cross-component/cross-component-service.token';

@Component({
    selector: 'permutr-service-output',
    templateUrl: './service-output.component.html',
    styleUrls: [ './service-output.component.scss' ]
})
export class PermutrServiceOutputComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _service: ICrossComponentService;

    private _content: string;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get content(): string {
        return this._content;
    }

    public set content(value: string) {
        this._content = value;
    }

    // Constructor ********************************************************************************
    constructor(@Inject(CrossComponentServiceToken) service: ICrossComponentService) {
        this._service = service;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
    }

    private format(content: string): string {
        if (content !== undefined) {
            const now = new Date();
            return now.toLocaleTimeString() + ': ' + content;
        }

        return undefined;
    }

    private subscribe(): void {
        if (this._service !== undefined) {
            this._subscriptions.push(this._service.message$.subscribe(this.handleContentChanged.bind(this)));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Handlers *****************************************************************************
    public handleClearClicked(event: MouseEvent): void {
        if (this._service !== undefined) {
            this._service.clear();
        }
    }

    private handleContentChanged(content: string): void {
        this.content = this.format(content);
    }
}
