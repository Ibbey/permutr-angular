import { Component, Input, Inject } from '@angular/core';

import { ICrossComponentService } from '@services/cross-component/cross-component-service.contract';
import { CrossComponentServiceToken } from '@services/cross-component/cross-component-service.token';

@Component({
    selector: 'permutr-service-input',
    templateUrl: './service-input.component.html',
    styleUrls: [ './service-input.component.scss' ]
})
export class PermutrServiceInputComponent {
    // Member Variables ***************************************************************************
    private readonly _service: ICrossComponentService;
    private _message: string;

    // Public Properties **************************************************************************
    @Input() public get message(): string {
        return this._message;
    }

    public set message(value: string) {
        this._message = value;
    }

    // Constructor ********************************************************************************
    constructor(@Inject(CrossComponentServiceToken) service: ICrossComponentService) {
        this._service = service;
    }

    // Private Methods ****************************************************************************

    // Event Handlers *****************************************************************************
    public handleSubmitClicked(event: MouseEvent): void {
        if (this._message !== undefined) {
            this._service.send(this._message);
            this.message = undefined;
        }
    }
}
