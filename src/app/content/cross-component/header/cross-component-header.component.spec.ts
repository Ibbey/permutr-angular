import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrCrossComponentHeaderComponent } from './cross-component-header.component';
import { AppModule } from '../../../app.module';

describe('PermutrCrossComponentHeaderComponent', () => {
    let component: PermutrCrossComponentHeaderComponent;
    let fixture: ComponentFixture<PermutrCrossComponentHeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrCrossComponentHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
