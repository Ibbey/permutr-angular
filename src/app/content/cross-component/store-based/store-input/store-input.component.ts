import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { PermutrGlobalState } from '@store/state/permutr.state';

import * as ACTIONS from '@store/actions/cross-component.actions';

@Component({
    selector: 'permutr-store-input',
    templateUrl: './store-input.component.html',
    styleUrls: [ './store-input.component.scss' ]
})
export class PermutrStoreInputComponent {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;
    private _message: string;

    // Public Properties **************************************************************************
    @Input() public get message(): string {
        return this._message;
    }

    public set message(value: string) {
        this._message = value;
    }

    // Constructor ********************************************************************************
    constructor(store: Store<PermutrGlobalState>) {
        this._store = store;
    }

    // Event Handlers *****************************************************************************
    public handleSubmitClicked(event: MouseEvent): void {
        this._store.dispatch(new ACTIONS.SetMessageAction(this._message));
        this.message = undefined;
    }
}
