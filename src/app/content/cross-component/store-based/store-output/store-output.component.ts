import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrGlobalState } from '@store/state/permutr.state';
import * as fromCrossComponent from '@store/selectors/cross-component.selectors';
import * as ACTIONS from '@store/actions/cross-component.actions';

@Component({
    selector: 'permutr-store-output',
    templateUrl: './store-output.component.html',
    styleUrls: [ './store-output.component.scss' ]
})
export class PermutrStoreOutputComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _content$: Observable<string>;
    private _content: string;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get content(): string {
        return this._content;
    }

    public set content(value: string) {
        this._content = value;
    }

    // Constructor ********************************************************************************
    constructor(store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];

        this._content$ = this._store.pipe(select(fromCrossComponent.getMessage));
    }

    private format(content: string): string {
        if (content !== undefined) {
            const now = new Date();
            return now.toLocaleTimeString() + ': ' + content;
        }

        return undefined;
    }

    private subscribe(): void {
        if (this._content$ !== undefined) {
            this._subscriptions.push(this._content$.subscribe(this.handleContentChanged.bind(this)));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Handlers *****************************************************************************
    public handleClearClicked(event: MouseEvent): void {
        this._store.dispatch(new ACTIONS.ClearMessageAction());
    }

    private handleContentChanged(content: string): void {
        this.content = this.format(content);
    }
}
