import { Component, Input, Output, EventEmitter, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { PermutrChildInputComponent } from '../child-input/child-input.component';
import { PermutrChildOutputComponent } from '../child-output/child-output.component';


@Component({
    selector: 'permutr-parent-child-container',
    templateUrl: './parent-child.component.html',
    styleUrls: [ './parent-child.component.scss' ]
})
export class PermutrParentChildComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    @ViewChild('input') private _input: PermutrChildInputComponent;
    @ViewChild('output') private _output: PermutrChildOutputComponent;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
    }

    private format(message: string): string {
        if (message !== undefined && message.length !== 0) {
            const now = new Date();
            return now.toLocaleTimeString() + ': ' + message;
        }

        return undefined;
    }

    private subscribe(): void {
        if (this._input !== undefined) {
            this._subscriptions.push(this._input.messageChanged.subscribe(this.handleMessageChanged.bind(this)));
        }
        if (this._output !== undefined) {
            this._subscriptions.push(this._output.clearClick.subscribe(this.handleClearClicked.bind(this)));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
        }
    }

    // Event Handlers *****************************************************************************
    private handleMessageChanged(message: string): void {
        if (this._output !== undefined) {
            this._output.content = this.format(message);
        }
    }

    private handleClearClicked(): void {
        if (this._input !== undefined) {
            this._input.message = undefined;
        }
    }
}
