import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'permutr-child-input',
    templateUrl: './child-input.component.html',
    styleUrls: [ './child-input.component.scss' ]
})
export class PermutrChildInputComponent {
    // Member Variables ***************************************************************************
    private _message: string;

    // Public Properties **************************************************************************
    @Input() public get message(): string {
        return this._message;
    }

    public set message(value: string) {
        this._message = value;
    }

    // Output *************************************************************************************
    @Output() public messageChanged: EventEmitter<string> = new EventEmitter<string>();

    // Constructor ********************************************************************************
    constructor() {

    }

    // Private Methods ****************************************************************************

    // Event Handlers *****************************************************************************
    public handleSubmitClicked(event: MouseEvent): void {
        if (this._message !== undefined) {
            this.messageChanged.emit(this._message);
            this.message = undefined;
        }
    }
}
