import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'permutr-child-output',
    templateUrl: './child-output.component.html',
    styleUrls: [ './child-output.component.scss' ]
})
export class PermutrChildOutputComponent {
    // Member Variables ***************************************************************************
    private _content: string;

    // Public Properties **************************************************************************
    @Input() public get content(): string {
        return this._content;
    }

    public set content(value: string) {
        this._content = value;
    }

    // Output *************************************************************************************
    @Output() public clearClick: EventEmitter<void> = new EventEmitter<void>();

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {

    }

    private clear(): void {
        this.content = undefined;
    }

    // Event Handlers *****************************************************************************
    public handleClearClicked(event: MouseEvent): void {
        this.clear();
        this.clearClick.emit();
    }
}
