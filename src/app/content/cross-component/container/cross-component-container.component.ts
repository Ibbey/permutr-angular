import { Component } from '@angular/core';

@Component({
    selector: 'permutr-content-cross-component',
    templateUrl: './cross-component-container.component.html',
    styleUrls: [ './cross-component-container.component.scss' ]
})
export class PermutrCrossComponentContainerComponent {

}
