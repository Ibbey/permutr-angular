import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrCrossComponentContainerComponent } from './cross-component-container.component';
import { AppModule } from '../../../app.module';

describe('PermutrCrossComponentContainerComponent', () => {
    let component: PermutrCrossComponentContainerComponent;
    let fixture: ComponentFixture<PermutrCrossComponentContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrCrossComponentContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
