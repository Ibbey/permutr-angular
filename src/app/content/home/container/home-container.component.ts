import { Component, Inject, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { environment } from '@env/environment';

import { EchoServiceToken } from '@services/echo/echo-service.token';
import { IEchoService } from '@services/echo/echo-service.contract';

@Component({
    selector: 'permutr-content-home',
    templateUrl: './home-container.component.html',
    styleUrls: [ './home-container.component.scss' ]
})
export class PermutrHomeContainerComponent implements OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _service: IEchoService;

    private _version: string;
    private _developer: string;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get version(): string {
        return this._version;
    }

    public get developer(): string {
        return this._developer;
    }

    // Constructor ********************************************************************************
    constructor (@Inject(EchoServiceToken) service: IEchoService) {
        this._service = service;
        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
        this._version = 'Current Version: ' + environment.version;
        this._developer = 'Developed By: Ryan Barriger (ryan.barriger@pimco.com)';

    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Hanlders *****************************************************************************
    public handleGithubRepoClicked(event: MouseEvent): void {
        // this._subscriptions.push(this._service.pingPython('PECHO').subscribe(x => console.log(x)));
    }
}
