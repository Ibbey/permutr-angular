import { Component } from '@angular/core';

@Component({
    selector: 'permutr-content-marketplace',
    templateUrl: './market-place-container.component.html',
    styleUrls: [ './market-place-container.component.scss' ]
})
export class PermutrMarketplaceContainerComponent {

}
