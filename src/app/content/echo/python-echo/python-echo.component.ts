import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { IEcho } from '@models/echo/_echo.barrel';

import { PermutrGlobalState } from '@store/state/permutr.state';

import * as fromEcho from '@store/selectors/echo.selectors';
import * as ECHO_ACTIONS from '@store/actions/echo.actions';

@Component({
    selector: 'permutr-python-echo',
    templateUrl: './python-echo.component.html',
    styleUrls: [ './python-echo.component.scss' ]
})
export class PermutrPythonEchoComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _message: string;

    private _itemsSource$: Observable<IEcho[]>;
    private _itemsSource: IEcho[];

    private _displayedColumns: string[];

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get message(): string {
        return this._message;
    }

    public set message(value: string) {
        this._message = value;
    }

    public get itemsSource(): IEcho[] {
        return this._itemsSource;
    }

    public get displayedColumns(): string[] {
        return this._displayedColumns;
    }

    // Constructor ********************************************************************************
    constructor(store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];

        this._displayedColumns = [
            'id',
            'message',
            'source',
            'timestamp'
        ];

        this._itemsSource$ = this._store.pipe(select(fromEcho.getPython));
    }

    private clear(): void {
        this._itemsSource = undefined;
    }

    private subscribe(): void {
        if (this._itemsSource$ !== undefined) {
            this._subscriptions.push(this._itemsSource$.subscribe(this.handleItemsSourceChanged.bind(this)));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Handlers *****************************************************************************
    public handleSubmitClicked(event: MouseEvent) {
        this._store.dispatch(new ECHO_ACTIONS.RequestPythonEchoAction(this.message));
        this.message = undefined;
    }

    private handleItemsSourceChanged(source: IEcho[]): void {
        this.clear();
        this._itemsSource = source;
    }
}
