import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrGlobalState } from '@store/state/permutr.state';
import * as fromPatterns from '@store/selectors/patterns.selectors';
import * as ACTIONS from '@store/actions/patterns.actions';

const RETRIEVING = 'Retrieving Asynchronous Data...';
const RECEIVED = 'Data acquisition successful!';

@Component({
    selector: 'permutr-dependency-chain',
    templateUrl: './dependency-chain.component.html',
    styleUrls: [ './dependency-chain.component.scss' ]
})
export class PermutrDependencyChainComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _statusA: string;
    private _statusB: string;
    private _statusC: string;

    private _chainA$: Observable<any>;
    private _chainA: any;

    private _chainB$: Observable<any>;
    private _chainB: any;

    private _chainC$: Observable<any>;
    private _chainC: any;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get chainA(): any {
        return this._chainA;
    }

    public get chainB(): any {
        return this._chainB;
    }

    public get chainC(): any {
        return this._chainC;
    }

    public get statusA(): string {
        return this._statusA;
    }

    public get statusB(): string {
        return this._statusB;
    }

    public get statusC(): string {
        return this._statusC;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];

        this._chainA$ = this._store.pipe(select(fromPatterns.getChainA));
        this._chainB$ = this._store.pipe(select(fromPatterns.getChainB));
        this._chainC$ = this._store.pipe(select(fromPatterns.getChainC));
    }

    private clear(): void {
        this._statusA = undefined;
        this._statusB = undefined;
        this._statusC = undefined;

        this._chainA = undefined;
        this._chainB = undefined;
        this._chainC = undefined;

        this._store.dispatch(new ACTIONS.ClearAllAction());
    }

    private subscribe(): void {
        this._subscriptions.push(this._chainA$.subscribe(x => {
            if (x !== undefined) {
                this._statusA = RECEIVED;
                this._statusB = RETRIEVING;
                this._chainA = x;
            }
        }));

        this._subscriptions.push(this._chainB$.subscribe(x => {
            if (x !== undefined) {
                this._statusB = RECEIVED;
                this._statusC = RETRIEVING;
                this._chainB = x;
            }
        }));

        this._subscriptions.push(this._chainC$.subscribe(x => {
            if (x !== undefined) {
                this._statusC = RECEIVED;
                this._chainC = x;
            }
        }));
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Handlers *****************************************************************************
    public handleExecuteClicked(event: MouseEvent): void {
        this.clear();
        this._statusA = RETRIEVING;
        this._store.dispatch(new ACTIONS.InitChainDependencyAction());
    }
}
