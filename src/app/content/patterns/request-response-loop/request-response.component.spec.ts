import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrRequestResponseLoopComponent } from './request-response.component';
import { AppModule } from '../../../app.module';

describe('PermutrRequestResponseLoopComponent', () => {
    let component: PermutrRequestResponseLoopComponent;
    let fixture: ComponentFixture<PermutrRequestResponseLoopComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrRequestResponseLoopComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
