import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrGlobalState } from '@store/state/permutr.state';
import * as fromPatterns from '@store/selectors/patterns.selectors';
import * as ACTIONS from '@store/actions/patterns.actions';
import { coerceNumber } from '@common/common.functions';

const RETRIEVING = 'Awaiting responses';

@Component({
    selector: 'permutr-request-response-loop',
    templateUrl: './request-response.component.html',
    styleUrls: [ './request-response.component.scss' ]
})
export class PermutrRequestResponseLoopComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _status: string;
    private _isRunning: boolean;

    private _requests: number[];
    private _selectedRequest: number;

    private _items$: Observable<any[]>;
    private _items: any[];

    private _queue$: Observable<any[]>;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get status(): string {
        return this._status;
    }

    public get isRunning(): boolean {
        return this._isRunning;
    }

    public get requests(): number[] {
        return this._requests;
    }

    public get selectedRequest(): number {
        return this._selectedRequest;
    }

    public set selectedRequest(value: number) {
        this._selectedRequest = coerceNumber(value);
    }

    public get items(): any[] {
        return this._items;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
        this._isRunning = false;
        this._requests = [
            1,
            2,
            3,
            4,
            5,
            6
        ];

        this._items$ = this._store.pipe(select(fromPatterns.getResponse));
        this._queue$ = this._store.pipe(select(fromPatterns.getRequests));
    }

    private clear(): void {
        this._status = undefined;
        this._isRunning = false;
        this._items = undefined;

        this._store.dispatch(new ACTIONS.ClearAllAction());
    }

    private subscribe(): void {
        this._subscriptions.push(this._items$.subscribe(x => this._items = x));
        this._subscriptions.push(this._queue$.subscribe(x => {
            if (x !== undefined) {
                this._status = RETRIEVING + ' (Queue Size: ' + x.length + ')';
                if (x.length === 0) {
                    this._isRunning = false;
                }
            }
        }));
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    private getServiceRequests(size: number): any[] {
        const reqs = [];

        for (let i = 1; i <= size; i++) {
            reqs.push({
                id: i,
                key: 'Request Key for ID: ' + i
            });
        }

        return reqs;
    }

    // Event Handlers *****************************************************************************
    public handleExecuteClicked(event: MouseEvent): void {
        if (this._selectedRequest !== undefined) {
            this.clear();
            this._status = RETRIEVING;
            this._isRunning = true;
            this._store.dispatch(new ACTIONS.InitRequestResponseLoopAction(this.getServiceRequests(this._selectedRequest)));
        }
    }
}
