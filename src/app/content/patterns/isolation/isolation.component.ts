import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrGlobalState } from '@store/state/permutr.state';
import * as fromPatterns from '@store/selectors/patterns.selectors';
import * as ACTIONS from '@store/actions/patterns.actions';

const RETRIEVING = 'Retrieving Asynchronous Data...';
const RECEIVED = 'Data acquisition successful!';

@Component({
    selector: 'permutr-isolation',
    templateUrl: './isolation.component.html',
    styleUrls: [ './isolation.component.scss' ]
})
export class PermutrIsolationComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _statusA: string;
    private _statusB: string;

    private _isolationA$: Observable<any>;
    private _isolationA: any;

    private _isolationB$: Observable<any>;
    private _isolationB: any;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get statusA(): string {
        return this._statusA;
    }

    public get statusB(): string {
        return this._statusB;
    }

    public get isolationA(): any {
        return this._isolationA;
    }

    public get isolationB(): any {
        return this._isolationB;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];

        this._isolationA$ = this._store.pipe(select(fromPatterns.getIsolationA));
        this._isolationB$ = this._store.pipe(select(fromPatterns.getIsolationB));
    }

    private clear(): void {
        this._statusA = undefined;
        this._statusB = undefined;
        this._isolationA = undefined;
        this._isolationB = undefined;

        this._store.dispatch(new ACTIONS.ClearAllAction());
    }

    private subscribe(): void {
        this._subscriptions.push(this._isolationA$.subscribe(x => {
            if (x !== undefined) {
                this._statusA = RECEIVED;
                this._isolationA = x;
            }
        }));
        this._subscriptions.push(this._isolationB$.subscribe(x => {
            if (x !== undefined) {
                this._statusB = RECEIVED;
                this._isolationB = x;
            }
        }));
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Handlers *****************************************************************************
    public handleExecuteClicked(event: MouseEvent): void {
        this.clear();
        this._statusA = RETRIEVING;
        this._statusB = RETRIEVING;
        this._store.dispatch(new ACTIONS.InitIsolationAction());
    }
}
