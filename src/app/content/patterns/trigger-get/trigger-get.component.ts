import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrGlobalState } from '@store/state/permutr.state';
import * as fromPatterns from '@store/selectors/patterns.selectors';
import * as ACTIONS from '@store/actions/patterns.actions';

import { coerceBoolean } from '@common/common.functions';

const RETRIEVING = 'Retrieving Asynchronous Data...';
const RECEIVED = 'Data acquisition successful!';

@Component({
    selector: 'permutr-trigger-get',
    templateUrl: './trigger-get.component.html',
    styleUrls: [ './trigger-get.component.scss' ]
})
export class PermutrTriggerGetComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;

    private _status: string;

    private _logicFlag: boolean;

    private _valueA$: Observable<any>;
    private _valueA: any;
    private _valueB$: Observable<any>;
    private _valueB: any;
    private _valueC$: Observable<any>;
    private _valueC: any;
    private _valueD$: Observable<any>;
    private _valueD: any;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get status(): string {
        return this._status;
    }

    // @Input() public get logicFlag(): boolean {
    //     return this._logicFlag;
    // }

    // public set logicFlag(value: boolean) {
    //     this._logicFlag = coerceBoolean(value);
    // }

    public get valueA(): any {
        return this._valueA;
    }

    public get valueB(): any {
        return this._valueB;
    }

    public get valueC(): any {
        return this._valueC;
    }

    public get valueD(): any {
        return this._valueD;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<PermutrGlobalState>) {
        this._store = store;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
        this._logicFlag = false;

        this._valueA$ = this._store.pipe(select(fromPatterns.getTriggerGetA));
        this._valueB$ = this._store.pipe(select(fromPatterns.getTriggerGetB));
        this._valueC$ = this._store.pipe(select(fromPatterns.getTriggerGetC));
        this._valueD$ = this._store.pipe(select(fromPatterns.getTriggerGetD));
    }

    private clear(): void {
        this._status = undefined;
        this._valueA = undefined;
        this._valueB = undefined;
        this._valueC = undefined;
        this._valueD = undefined;

        this._store.dispatch(new ACTIONS.ClearAllAction());
    }

    private subscribe(): void {
        this._subscriptions.push(this._valueA$.subscribe(x => {
            if (x !== undefined) {
                this._status = RECEIVED;
                this._valueA = x;
            }
        }));

        this._subscriptions.push(this._valueB$.subscribe(x => {
            if (x !== undefined) {
                this._status = RECEIVED;
                this._valueB = x;
            }
        }));

        this._subscriptions.push(this._valueC$.subscribe(x => {
            if (x !== undefined) {
                this._status = RECEIVED;
                this._valueC = x;
            }
        }));

        this._subscriptions.push(this._valueD$.subscribe(x => {
            if (x !== undefined) {
                this._status = RECEIVED;
                this._valueD = x;
            }
        }));
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    // Event Handlers *****************************************************************************
    public handleExecuteClicked(event: MouseEvent): void {
        this.clear();
        this._status = RETRIEVING;

        this._store.dispatch(new ACTIONS.InitTriggerGetAction(this._logicFlag));
    }

    public handleSlideToggleChanged(event: MouseEvent): void {
        this._logicFlag = !this._logicFlag;
    }
}
