import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrInventoryAnalyticsComponent } from './inventory-analytics.component';
import { AppModule } from '../../../app.module';

describe('PermutrInventoryAnalyticsComponent', () => {
    let component: PermutrInventoryAnalyticsComponent;
    let fixture: ComponentFixture<PermutrInventoryAnalyticsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrInventoryAnalyticsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
