import { Component, ViewChild, AfterContentInit, OnInit, OnDestroy } from '@angular/core';
import { MatButtonToggleGroup, MatDialog, MatDialogRef } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { PermutrInventoryDialogComponent } from '../dialog/inventory-dialog.component';

import { PermutrGlobalState } from '@store/state/permutr.state';

import { InventoryCollection, InventoryItem } from '@models/inventory/_inventory.barrel';

import * as fromInventory from '@store/selectors/inventory.selectors';
import * as INV_ACTIONS from '@store/actions/inventory.actions';



@Component({
    selector: 'permutr-inventory-collection',
    templateUrl: './inventory-collection.component.html',
    styleUrls: [ './inventory-collection.component.scss' ]
})
export class PermutrInventoryCollectionComponent implements AfterContentInit, OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<PermutrGlobalState>;
    private readonly _dialog: MatDialog;

    private _displayedColumns: string[];
    private _itemsSource: InventoryItem[];

    private _inventory$: Observable<InventoryCollection>;
    private _intentory: InventoryCollection;

    private _filters: string[];

    private _subscriptions: Subscription[];

    private _dialogRef: MatDialogRef<PermutrInventoryDialogComponent>;

    @ViewChild('group') private _toggleGroup: MatButtonToggleGroup;

    // Public Properties **************************************************************************
    public get displayedColumns(): string[] {
        return this._displayedColumns;
    }

    public get itemsSource(): InventoryItem[] {
        return this._itemsSource;
    }

    public set itemsSource(value: InventoryItem[]) {
        this._itemsSource = value;
    }

    public get filters(): string[] {
        return this._filters;
    }

    public set filters(value: string[]) {
        this._filters = value;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<PermutrGlobalState>, dialog: MatDialog) {
        this._store = store;
        this._dialog = dialog;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngAfterContentInit(): void {
        this._store.dispatch(new INV_ACTIONS.SetInventoryFilterAction('All'));
        this._toggleGroup.value = 'All';
    }

    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];
        this._displayedColumns = this.getDisplayedColumns();
        this._filters = this.getFilters();

        this._inventory$ = this._store.pipe(select(fromInventory.getInventoryCollection));
    }

    private openDialog(): void {
        this._dialogRef = this._dialog.open(PermutrInventoryDialogComponent, undefined);

        this._subscriptions.push(this._dialogRef.afterClosed().subscribe(this.handleDialogClosed.bind(this)));
    }

    private addInventoryItem(item: InventoryItem): void {

    }

    private clear(): void {
        this.itemsSource = undefined;
    }

    private subscribe(): void {
        if (this._inventory$ !== undefined) {
            this._subscriptions.push(this._inventory$.subscribe(this.handleInventoryCollectionChanged.bind(this)));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }

    private getDisplayedColumns(): string[] {
        return [
           'type',
           'name',
           'category',
           'description',
           'cost',
           'estimatedValue',
           'actualValue',
           'acquisition',
           'status'
        ];
    }

    private getFilters(): string[] {
        return [
            'All',
            'Mine'
        ];
    }

    // Event Handlers *****************************************************************************
    public handleFilterClicked(event: MouseEvent, filter: string): void {
        this._store.dispatch(new INV_ACTIONS.SetInventoryFilterAction(filter));
    }

    public handleAddItemClicked(event: MouseEvent): void {
        this.openDialog();
    }

    private handleInventoryCollectionChanged(collection: InventoryCollection): void {
        this.clear();

        if (collection !== undefined) {
            this.itemsSource = collection.items;
        }
    }

    private handleDialogClosed(result?: InventoryItem): void {
        if (result !== undefined) {
            this.addInventoryItem(result);
        }
    }
}
