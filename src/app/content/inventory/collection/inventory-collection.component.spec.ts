import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrInventoryCollectionComponent } from './inventory-collection.component';
import { AppModule } from '../../../app.module';

describe('PermutrInventoryCollectionComponent', () => {
    let component: PermutrInventoryCollectionComponent;
    let fixture: ComponentFixture<PermutrInventoryCollectionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrInventoryCollectionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
