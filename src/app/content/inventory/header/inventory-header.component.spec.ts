import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { PermutrInventoryHeaderComponent } from './inventory-header.component';
import { AppModule } from '../../../app.module';

describe('PermutrInventoryHeaderComponent', () => {
    let component: PermutrInventoryHeaderComponent;
    let fixture: ComponentFixture<PermutrInventoryHeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PermutrInventoryHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
