import { Component, Input, Inject, AfterContentInit, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { InventoryItem, IInventoryItem, toInventoryStatus, toInventoryType  } from '@models/inventory/inventory-item';

import { coerceNumber, coerceBoolean } from '@common/common.functions';

import { InventoryStatusOptions, InventoryTypes } from '@common/common.constants';

@Component({
    selector: 'permutr-inventory-dialog',
    templateUrl: './inventory-dialog.component.html',
    styleUrls: [ './inventory-dialog.component.scss' ]
})
export class PermutrInventoryDialogComponent implements AfterContentInit, OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _dialogRef: MatDialogRef<PermutrInventoryDialogComponent>;
    private readonly _item: InventoryItem;
    private readonly _ownerId: string;

    private _name: string;
    private _category: string;
    private _type: string;
    private _acquisition: Date;
    private _description: string;
    private _cost: number;
    private _estimatedValue: number;
    private _actualValue: number;
    private _location: string;
    private _status: string;
    private _public: boolean;

    private _submitDisabled: boolean;

    private _acquisitionInputControl: FormControl;

    // Public Properties **************************************************************************
    @Input() public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
        this.validate();
    }

    @Input() public get category(): string {
        return this._category;
    }

    public set category(value: string) {
        this._category = value;
        this.validate();
    }

    @Input() public get type(): string {
        return this._type;
    }

    public set type(value: string) {
        this._type = value;
        this.validate();
    }

    @Input() public get acquisition(): Date {
        return this._acquisition;
    }

    public set acquisition(value: Date) {
        this._acquisition = value;
        this.validate();
    }

    @Input() public get description(): string {
        return this._description;
    }

    public set description(value: string) {
        this._description = value;
        this.validate();
    }

    @Input() public get cost(): number {
        return this._cost;
    }

    public set cost(value: number) {
        this._cost = coerceNumber(value);
        this.validate();
    }

    @Input() public get estimatedValue(): number {
        return this._estimatedValue;
    }

    public set estimatedValue(value: number) {
        this._estimatedValue = coerceNumber(value);
        this.validate();
    }

    @Input() public get actualValue(): number {
        return this._actualValue;
    }

    public set actualValue(value: number) {
        this._actualValue = coerceNumber(value);
        this.validate();
    }

    @Input() public get location(): string {
        return this._location;
    }

    public set location(value: string) {
        this._location = value;
        this.validate();
    }

    @Input() public get status(): string {
        return this._status;
    }

    public set status(value: string) {
        this._status = value;
        this.validate();
    }

    @Input() public get public(): boolean {
        return this._public;
    }

    public set public(value: boolean) {
        this._public = coerceBoolean(value);
    }

    public get acquisitionInputControl(): FormControl {
        return this._acquisitionInputControl;
    }

    public get categoryTypes(): string[] {
        return InventoryTypes;
    }

    public get statusOptions(): string[] {
        return InventoryStatusOptions;
    }

    @Input() public get submitDisabled(): boolean {
        return this._submitDisabled;
    }

    public set submitDisabled(value: boolean) {
        this._submitDisabled = coerceBoolean(value);
    }

    // Constructor ********************************************************************************
    constructor (dialogRef: MatDialogRef<PermutrInventoryDialogComponent>,
                 @Inject(MAT_DIALOG_DATA) data?: any) {
        this._dialogRef = dialogRef;
        if (data) {
            this._item = data.item;
            this._ownerId = data.ownerId;
        }

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngAfterContentInit(): void {
        this.load();
    }

    public ngOnInit(): void {

    }

    public ngOnDestroy(): void {

    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._acquisitionInputControl = new FormControl();
        this.submitDisabled = true;
    }

    private load(): void {

    }

    private validate(): void {
        if (this._acquisition !== undefined && this._actualValue !== undefined && this._category !== undefined &&
            this._cost !== undefined && this._description !== undefined && this._estimatedValue !== undefined &&
            this._location !== undefined && this._name !== undefined && this._status !== undefined &&
            this._type !== undefined) {
                this.submitDisabled = false;
            } else {
                this.submitDisabled = true;
            }
    }

    private closeDialog(item?: InventoryItem): void {
        if (this._dialogRef !== undefined) {
            this._dialogRef.close(item);
        }
    }

    private getItem(): InventoryItem {
        return new InventoryItem(<IInventoryItem>{
            ownerId: this._ownerId,
            type: toInventoryType(this.type),
            name: this.name,
            category: this.category,
            description: this.description,
            acquisition: this.acquisitionInputControl.value,
            cost: this.cost,
            estimatedValue: this.estimatedValue,
            actualValue: this.actualValue,
            location: this.location,
            status: toInventoryStatus(this.status),
            public: this.public
        });
    }

    // Event Handlers *****************************************************************************
    public handleCloseDialogClicked(event: MouseEvent): void {
        this.closeDialog();
    }

    public handleCancelClicked(event: MouseEvent): void {
        this.closeDialog();
    }

    public handleSubmitClicked(event: MouseEvent): void {
        this.closeDialog(this.getItem());
    }
}
