module.exports = {
    preset: 'jest-preset-angular',
    roots: [ 'src' ],
    setupFiles: [
        '<rootDir>/node_modules/hammerjs/hammer.js'
    ],
    setupFilesAfterEnv: [
        '<rootDir>/src/setup-jest.ts'
    ],
    moduleNameMapper: {
        '@env/(.*)': '<rootDir>/src/environments/$1',
        '@assets/(.*)': '<rootDir>/src/assets/$1',
        '@common/(.*)': '<rootDir>/src/app/common/$1',
        '@models/(.*)': '<rootDir>/src/app/models/$1',
        '@store/(.*)': '<rootDir>/src/app/store/$1',
        '@services/(.*)': '<rootDir>/src/app/services/$1',
    }
}