FROM nginx:alpine

ARG TARGET_ENV=dev

COPY nginx/nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*

COPY dist/$TARGET_ENV /usr/share/nginx/html

CMD [ "nginx", "-g", "daemon off; " ]
